#include <iostream>
#include <vector>

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <typename T>
class LinkedList
{
    struct node {
        T data;
        node* link;
    };

    node* first;


public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

    class iterator;
    class const_iterator;

    LinkedList() : first(new node)
    {
        first->link=nullptr;
    };

    LinkedList(std::initializer_list<T> list);

    ~LinkedList()
    {
        node* current = first;
        while (current != nullptr)
        {
            node* next = current->link;
            delete current;
            current = next;
        }
    }

    void push_front(T &&value);
    void push_front(const T &value);
    reference front();
    const_reference front() const;
    bool empty() const;
    size_type size() const;
    iterator begin();
    iterator end();
    iterator erase_after(const_iterator position);
    const_iterator begin() const;
    const_iterator end() const;

};

template <typename T>
class LinkedList<T>::iterator:std::iterator<std::forward_iterator_tag, T>
    {
        friend LinkedList;
        node* position;
        public:
            iterator(): position(nullptr) {};
            iterator(node* current) : position(current){};
            iterator(const iterator& otherIterator )
            {
                this->position=otherIterator.position;
            }
            iterator& operator= (const iterator& otherIterator)
            {
                this->position=otherIterator.position;
                return *this;

            }

            bool operator== (const iterator& otherIterator) const
            {
                return position == otherIterator.position;
            }
            bool operator!= (const iterator& otherIterator)
            {
                return position != otherIterator.position;
            }
            ~iterator(){};

            reference operator* ()
            {
                return position->data;
            }

            reference operator-> ()
            {
                return *position->data;
            }

            iterator& operator++ () //prefix
            {
                position = position->link;
                return *this;
            }

            iterator operator++ (int)   //Postfix
            {
                iterator current_iterator(*this);
                ++(*this);
                return current_iterator;
            }
            bool isLastNode()
            {
                return position->link==nullptr;
            }
    };

template <typename T>
class LinkedList<T>::const_iterator:std::iterator<std::forward_iterator_tag, const T>
    {
        friend LinkedList;
        node* position;
        public:
            const_iterator(): position(nullptr) {};
            const_iterator(node* current) : position(current){};
            const_iterator(const const_iterator& otherIterator )
            {
                this->position=otherIterator.position;
            }
            const_iterator(const iterator& otherIterator )
            {
                this->position=otherIterator.position;
            }
            const_iterator& operator= (const const_iterator& otherIterator)   //
            {
                this->position=otherIterator.position;
                return *this;

            }

            bool operator== (const const_iterator& otherIterator)
            {
                return position == otherIterator.position;
            }
            bool operator!= (const const_iterator& otherIterator)
            {
                return position != otherIterator.position;
            }
            ~const_iterator(){};

            const_reference operator* ()
            {
                return position->data;
            }

            const_reference operator-> ()
            {
                return *position->data;
            }

            const_iterator& operator++ ()
            {
                position = position->link;
                return *this;
            }

            const_iterator operator++ (int)
            {
                 const_iterator current_iterator(*this);
                ++(*this);
                return current_iterator;
            }
            bool isLastNode()
            {
                return position->link==nullptr;
            }
    };

//----------------------------------------------------------------------------------------------------------
//---------------Methoden-----------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------

template <typename T>
LinkedList<T>::LinkedList(std::initializer_list<T> list) : LinkedList()
{
    std::vector<T> vectorList {list};
    for(auto it=vectorList.size(); it>0; --it)
    {
        this->push_front(vectorList[it-1]);
    }

}
template <typename T>
void LinkedList<T>::push_front(T &&value)
{
    node* newNode = new node{value, first};
    first=newNode;
}

template <typename T>
void LinkedList<T>::push_front(const T &value)
{
    node* newNode = new node{value, first};
    first=newNode;
}

template <typename T>
typename LinkedList<T>::reference LinkedList<T>::front()
{
    return first->data;
}

template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::front() const
{
    return first->data;
}

template <typename T>
bool LinkedList<T>::empty() const
{
    return begin() == end();
}

template <typename T>
typename LinkedList<T>::size_type LinkedList<T>::size() const
{
    size_type x =0;
    for(auto i=begin(); i!=end(); ++i )
    {
        x++;
    }
    return x;
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    iterator it(first);
    return it;
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::begin() const
{
    const_iterator it(first);
    return it;
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::erase_after(const_iterator position)
{
    node* current=position.position;
    if(current->link==nullptr)
    {
        std::cout << "Nothing to delete. This is the last value of the List" << std::endl;
    }
    node* next=position.position->link;
    if(next->link==nullptr)
    {
        return end();
    }
    else
    {
       current->link=next->link;
        ++position;
        delete next;
        return iterator(position.position);
    }


}
template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    iterator endOfList(first);
    while(!endOfList.isLastNode())
    {
        ++endOfList;
    }
        return endOfList;
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::end() const
{
    const_iterator endOfList(first);
    while(!endOfList.isLastNode())
    {
        ++endOfList;
    }
        return endOfList;
}
#endif // LINKEDLIST_H
