
#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

template <typename T>
class LinkedList
{
public:

    class node
    {
    public:
        node(const T &x):data(x),next(NULL) {}
        T      data;
        node  *next;
    };

    typedef node* node_ptr;
    typedef node const * node_ptr_const;

    node_ptr head = nullptr;
    node_ptr tail = nullptr;
    node_ptr current = nullptr;

    LinkedList(const LinkedList& List) = delete;

    LinkedList operator=(const LinkedList& x) = delete;

    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

    LinkedList()
    {
        head = nullptr;
        tail = nullptr;
        current = nullptr;
    }

    ~LinkedList()
    {
        clear_LinkedList();
    }

    class iterator
    {
    private:

    public:
        node_ptr i=nullptr;
        iterator(node_ptr n)
        {
            i =  n;
        }

        iterator(const iterator& n)
        {
            i=n.i;
        }

        iterator& operator++()
        {
            i=i->next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator tmp(*this);
            i = i->next;
            return tmp;
        }

        reference operator*() const
        {
            return i->data;
        }

        bool operator==(const iterator& n) const
        {
            return i==n.i;
        }

        bool operator!=(const iterator& n) const
        {
            return i!=n.i;
        }
    };


    class const_iterator
    {
    private:

    public:
        node_ptr i = nullptr;
        const_iterator(node_ptr n)
        {
            i =  n;
        }

        const_iterator(const const_iterator& n)
        {
            i=n.i;
        }

        const_iterator(const iterator& n) : i(n.i)
        {
        }

        const_iterator& operator++()
        {
            i=i->next;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            i = i->next;
            return tmp;
        }

        const_reference operator*() const
        {
            return i->data;
        }

        bool operator==(const const_iterator& n) const
        {
            return i==n.i;
        }

        bool operator!=(const const_iterator& n) const
        {
            return i!=n.i;
        }
    };


    void push_front (T &&value);
    void push_front (const T &value);
    void push_back (const T &value);
    reference front ();
    const_reference front () const;
    bool empty () const;
    size_type size () const;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    void clear_LinkedList();
    iterator erase_after (const_iterator position);

    LinkedList(const std::initializer_list<T>& constList)
    {
        head = nullptr;
        current = nullptr;
        tail = nullptr;

        for (auto iter = constList.begin(); iter!=constList.end(); ++iter)
        {
            push_back(*iter);
        }
    }
};

template <typename T>
void LinkedList<T>::push_front(T&& value)
{
    node *tmp=head;
    head=new node(value);
    head->next = tmp;

    if(!tail)
    {
        tail=head;
    }

}

template <typename T>
void LinkedList<T>::push_front(const T &value)
{
    node *tmp=head;
    head=new node(value);
    head->next = tmp;

    if(!tail)
    {
        tail=head;
    }
}

template <typename T>
void LinkedList<T>::push_back(const T& value)
{
    node_ptr n = new node(value);
    n->next = nullptr;
    n->data = value;

    if (tail != nullptr)
    {
        current = n;
        tail -> next = current;
        tail = current;
    }

    else
    {
        head = n;
        tail = n;
    }
}

template<typename T >
typename LinkedList<T>::reference LinkedList< T >::front ()
{
    if ( !head )
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->data;
}

template<typename T >
typename LinkedList<T>::const_reference LinkedList< T >::front () const
{
    if ( !head )
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->data;
}

template <typename T>
bool LinkedList<T>::empty() const
{
    return (head==NULL);
}

template<typename T >
typename LinkedList<T>::size_type LinkedList< T >::size ( ) const
{
    unsigned int size = 0;
    node_ptr temp = head;
    while (temp != nullptr)
    {
        size++;
        temp = temp-> next;
    }
    return size;
}

template<typename T >
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    return iterator(head);
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    return iterator(tail->next);
}
template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::begin() const
{
    return const_iterator(head);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::end() const
{
    return const_iterator(tail->next);
}

template <typename T>
void LinkedList<T>::clear_LinkedList()
{
    while(head!=nullptr)
    {
        current = head->next;
        delete head;
        head = current;
    }
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::erase_after (const_iterator position)
{
    if(position!=tail)
    {
    node_ptr temp = position.i->next->next;   //Speichern des Knotens, der nach dem zu loeschenden Knoten kommt
    delete position.i->next;                  //Knoten wird geloescht
    position.i->next = temp;                  //temporaerer Knoten = neuer Knoten nach aktuellem Knoten
    if (temp == nullptr)
    {
        tail = position.i;
    }
    return iterator(temp);
    }else return iterator(tail->next);
}


#endif
