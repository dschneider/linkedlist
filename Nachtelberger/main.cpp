#include "LinkedList.h"
#include <algorithm>
#include <numeric>
#include <iostream>
using namespace std;
template <typename T>

void print(const LinkedList<T> & x)
{
	copy(x.begin(), x.end(),ostream_iterator<T>(cout,"\n"));
}

int main()
{
    //Funktions�berpr�fung
	LinkedList<int> list; //Liste bauen


	list.push_front(7);
	list.push_front(3);
	list.push_front(3);
	list.push_front(1);
	list.push_front(0); // Elemente werden an erster Stelle der Liste erstellt
    const int& val = 1337;
    list.push_front(val);

	cout << "Init Liste" << endl; //Ausgabe
	print(list);
    cout << "Size " << list.size() << endl;
    cout << endl;
}
