#ifndef Linkedlist_H
#define Linkedlist_H
#include <iterator>

template <typename T>
class LinkedList
{
	struct Node             //Definition der Node-Struktur
	{
		Node(const T& val, Node* next = 0) : data(val), nextNode(next)
		{ }
		T data;      //Dateilteil
		Node* nextNode;   //Adressteil
	};
	Node* last;

    public:


    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;
    class iterator;
    class const_iterator;

	LinkedList() : last(new Node(T()))
	{
	    last->nextNode = last;
    }
	LinkedList(std::initializer_list<T> list) : last(new Node(T()))
	{
	    Node** next = &last->nextNode;
	    for (auto&& data : list)
	    {
	        Node* item = new Node(data, last);
            *next = item;
            next = &item->nextNode;
	    }
	}

	~LinkedList()
	{
	    while (last->nextNode != last)
	    {
	        auto to_delete = last->nextNode;
	        last->nextNode = to_delete->nextNode;
	        delete to_delete;
	    }
	    delete last;
	}

	void push_front(const T &data)
	{
	    last->nextNode = new Node(data, last->nextNode);
	}

	size_type size() const
    {
        size_type x =0;
        for(const_iterator i=begin(); i!=end(); ++i )
        {
            x++;
        }
        return x;
    }
	bool empty() const
	{
	    return last == last->nextNode;
    }
	T& front()
	{
	    return *begin();
    }
	const T& front() const
	{
	    return *begin();
    }
    	iterator end()
	{
	    return iterator(last);
    }
	iterator begin()
	{
	    return iterator(last->nextNode);
    }

    	const_iterator end() const
	{
		return const_iterator(last);
	}
	const_iterator begin() const
	{
		return const_iterator(last->nextNode);
	}

	iterator erase_after(const_iterator position)
	{
	    Node* next = position.pos->nextNode;
	    position.pos->nextNode = next->nextNode;
	    delete next;
	    return iterator(position.pos->nextNode);
	}

};

template <typename T>
class LinkedList<T>::iterator: public std::iterator<std::forward_iterator_tag, T>
{
	Node* pos;
    public:
        friend class LinkedList;
        iterator():pos(0)
        {}
        iterator(Node* curr):pos(curr)
        {}
        iterator& operator=(const iterator& otherIter)
        {
            pos=otherIter.pos;
            return *this;
        }
        reference operator->() const
        {
            return pos->nextNode;
        }
        bool operator==(const iterator& otherIter) const
        {
            return pos == otherIter.pos;
        }
        bool operator!=(const iterator& otherIter) const
		{
			return pos != otherIter.pos;
		}
		iterator& operator++()
		{
			pos = pos->nextNode;
			return *this;
		}
        iterator operator++(int)
        {
            auto old = iterator(*this);
            pos = pos->nextNode;
            return old;
        }
		reference operator*() const
		{
		    return pos->data;
        }
};
template <typename T>
class LinkedList<T>::const_iterator : public std::iterator<std::forward_iterator_tag, const T>
{
    friend LinkedList;
	Node* pos;
    public:
        const_iterator(): pos(0)
        {}
        const_iterator(Node* curr) : pos(curr)
        {}
        const_iterator(iterator other) : pos(other.pos)
        {
        }
        const_iterator& operator++()
        {
            pos = pos->nextNode;
            return *this;
        }
        const_iterator operator++(int)
        {
            auto old = const_iterator(*this);
            pos = pos->nextNode;
            return old;
        }
        reference operator*() const
        {
            return pos->data;
        }
        bool operator!=(const const_iterator& otherIter) const
        {
            return pos != otherIter.pos;
        }
        const_iterator& operator=(const iterator& otherIter)
        {
            pos=otherIter.pos;
            return *this;
        }
};
#endif
