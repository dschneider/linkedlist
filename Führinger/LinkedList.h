
#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

template <typename T>
class LinkedList
{
public:

    class node
    {
    public:
        node(const T &data):value(data),next(NULL) {}
        T      value;
        node  *next;
    };

    LinkedList(const LinkedList& List) = delete;

    LinkedList operator=(const LinkedList& data) = delete;

    typedef node* node_ptr;
    typedef node const * node_ptr_const;

    node_ptr head = nullptr;
    node_ptr tail = nullptr;
    node_ptr current = nullptr;

    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

    LinkedList()
    {
        head = nullptr;
        tail = nullptr;
        current = nullptr;
    }

    ~LinkedList()
    {
        clear_LinkedList();
    }

    LinkedList(const std::initializer_list<T>& constList)
    {
        head = nullptr;
        current = nullptr;
        tail = nullptr;

        for (auto iter = constList.begin(); iter!=constList.end(); ++iter)
        {
            push_back(*iter);
        }
    }

    class iterator
    {
    public:
        node_ptr iter;
        iterator(node_ptr data=NULL):iter(data) {}
        iterator& operator++()
        {
            iter=iter->next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator tmp(*this);
            iter = iter->next;
            return tmp;
        }

        const int& operator*() const
        {
            return iter->value;
        }

        bool operator==(const iterator& data) const
        {
            return iter==data.iter;
        }

        bool operator!=(const iterator& data) const
        {
            return iter!=data.iter;
        }
    };


    class const_iterator
    {
    public:
        node_ptr iter;
        const_iterator(node_ptr data=NULL):iter(data) {}
        const_iterator(const iterator& other):iter(other.iter) {}
        const_iterator& operator++()
        {
            iter=iter->next;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            iter = iter->next;
            return tmp;
        }

        const int& operator*() const
        {
            return iter->value;
        }

        bool operator==(const const_iterator& data) const
        {
            return iter==data.iter;
        }

        bool operator!=(const const_iterator& data) const
        {
            return iter!=data.iter;
        }
    };


    void push_front (T &&value);
    void push_front (const T &value);
    void push_back (const T &value);
    reference front ();
    const_reference front () const;
    bool empty () const;
    size_type size () const;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    void clear_LinkedList();
    void pop_front();
    iterator erase_after(const_iterator position);
};

template <typename T>
void LinkedList<T>::push_front(T&& value)
{
    node_ptr tmp=head;
    head=new node(value);
    head->next = tmp;

    if(!tail)
    {
        tail=head;
    }

}

template <typename T>
void LinkedList<T>::push_front(const T &value)
{
    node_ptr tmp=head;
    head=new node(value);
    head->next = tmp;

    if(!tail)
    {
        tail=head;
    }
}

template <typename T>
void LinkedList<T>::push_back(const T& value)
{
    node_ptr n = new node(value);
    n->next = nullptr;
    n->value = value;

    if (tail != nullptr)
    {
        current = n;
        tail -> next = current;
        tail = current;
    }

    else
    {
        head = n;
        tail = n;
    }
}

template<typename T >
typename LinkedList<T>::reference LinkedList< T >::front ()
{
    if ( !head )
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->value;
}

template<typename T >
typename LinkedList<T>::const_reference LinkedList< T >::front () const
{
    if ( !head )
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->value;
}

template <typename T>
bool LinkedList<T>::empty() const
{
    return (head==NULL);
}

template<typename T >
typename LinkedList<T>::size_type LinkedList< T >::size ( ) const
{
    unsigned int size = 0;
    node_ptr temp = head;
    while (temp != nullptr)
    {
        size++;
        temp = temp-> next;
    }
    return size;
}

template<typename T >
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    return iterator(head);
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    return iterator(tail->next);
}
template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::begin() const
{
    return const_iterator(head);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::end() const
{
    return const_iterator(tail->next);
}

template <typename T>
void LinkedList<T>::clear_LinkedList()
{
    while(head!=nullptr)
    {
        current = head->next;
        delete head;
        head = current;
    }
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::erase_after(const_iterator position)
{
    if(position.iter)
    {
        node_ptr tmp = position.iter->next->next;
        delete position.iter->next;
        position.iter->next = tmp;
        if (tmp == nullptr)
        {
            tail = position.iter;
        }
        return iterator(tmp);
    }
    else
    {
        return iterator(position.iter);
    }
}

#endif //LINKEDLIST_H_INCLUDED
