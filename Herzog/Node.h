#ifndef NODE_H
#define NODE_H

template <class T>

struct Node             //Struktur wird definiert, wie sieht knoten aus? (zeiger, usw...)
{
	Node(const T& value, Node* next = 0) : valueNode(value), nextNode(next)  //das ist der konstruktor dazu; ... : hier wird valueNode und nextNode Variablen zugewiesen...
	{
	}


	T valueNode;      //attripute der struktur (z.B person --> alter, name,...)
	Node* nextNode;   //attripute der struktur (z.B person --> alter, name,...)
};

#endif // NODE_H
