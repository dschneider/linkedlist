#include "LinkedList.h"
#include <algorithm>
#include <numeric>
#include <iostream>

using namespace std;

template <typename T>                      //T enables the handover of any data type
void print(const LinkedList<T> & x)
{
	copy(x.begin(), x.end(), std::ostream_iterator<T>(std::cout,"\n"));
}

int main()
{
	LinkedList<int> list;                //List content
	list.push_front(10);
	list.push_front(20);
	list.push_front(30);
	list.push_front(40);

    const int& value = 50;
    list.push_front(value);

	cout << "Init Liste" << endl;
	print(list);
    cout << "Size " << list.size() << endl;
    cout << endl;

    cout << "Erase After" << endl;
    //LinkedList<int>::const_iterator it = std::find(list.begin(), list.end(), 20);
    //it = list.erase_after(it);

    print(list);
    cout << "Size " << list.size() << endl;
    cout << endl;
}
