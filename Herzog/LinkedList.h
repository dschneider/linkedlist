#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iterator>

template <typename T>
class LinkedList
{
	struct Node             //defines the structure of the node
	{
		Node(const T& value, Node* next = 0) : data(value), nextNode(next)
		{
		}

		T data;
		Node* nextNode;
	};

	Node* last;

public:
    typedef T value_type;                       //value type
    typedef value_type& reference;              //reference on value, access the first element. Returns a reference to the first element in the container. Calling front on an empty container is undefined. RETURNS reference to the first element
    typedef const value_type& const_reference;  //reference on value, access the first element. Returns a reference to the first element in the container. Calling front on an empty container is undefined. RETURNS reference to the first element
    typedef std::size_t size_type;

    class iterator;
    class const_iterator;

    LinkedList(std::initializer_list<T> list) : last(new Node(T()))
    {
	Node** next = &last->nextNode;
	for (auto&& data : list)
	{
	    Node* item = new Node(data, last);
            *next = item;
            next = &item->nextNode;
	}
    }

	LinkedList() : last(new Node(T()))
	{
	    last->nextNode = last; // initialize list; nextNode point to last
    }

	~LinkedList()
	{
	    while (last->nextNode != last)
	    {
	        auto to_delete = last->nextNode;
	        last->nextNode = to_delete->nextNode;
	        delete to_delete;
	    }
	    delete last;
	}

	void push_front(const T &data)      //inserts an element to the beginning. Prepends the given element value to the beginning of the container. No iterators or references are invalidated. PARAMETERS value the value of the element to prepend
	{
	    last->nextNode = new Node(data, last->nextNode);
	}

	void insert (iterator iter, const T& data)
	{
	    Node* tmp = new Node(data, iter.position->nextNode);
		if (iter.position == last)
			last = tmp;
		iter.position->nextNode = tmp;
	}

//void push_front(T &&value)


	size_type size() const      //unsigned integer type
    {
        size_type x =0;     //RETURNS number of element in the container
        for(const_iterator i=begin(); i!=end(); ++i )
        {
            x++;
        }
        return x;
    }

	bool empty() const                //checks whether the container is empty. Checks if the container has no elements, i.e. whether begin() == end(). RETURNS true if the container is empty, false otherwise
	{
	    return last == last->nextNode;
    }

	T& front()
	{
	    return *begin();
    }

	const T& front() const
	{
	    return *begin();
    }

	iterator begin()        //returns an iterator to the beginning Returns an iterator to the first element of the container. If the container is empty, the returned iterator will be equal to end(). RETURNS Iterator to the first element
	{
	    return iterator(last->nextNode);
    }

	iterator end()          //returns an iterator to the end. Returns an iterator to the element following the last element of the container. This element acts as a placeholder; attempting to access it results in undefined behavior. RETURNS Iterator to the element following the last element
	{
	    return iterator(last);
    }

	const_iterator begin() const        //returns an iterator to the beginning Returns an iterator to the first element of the container. If the container is empty, the returned iterator will be equal to end(). RETURNS Iterator to the first element
	{
		return const_iterator(last->nextNode);
	}

	const_iterator end() const      //returns an iterator to the end. Returns an iterator to the element following the last element of the container. This element acts as a placeholder; attempting to access it results in undefined behavior. RETURNS Iterator to the element following the last element
	{
		return const_iterator(last);
	}

    iterator erase_after(const_iterator position)
	{
	    if (!empty())
        {
            Node* temp = position.position->nextNode;
            if (position.position->nextNode)
            {
                position.position->nextNode = position.position->nextNode->nextNode;
            }
            delete temp;
	    }
	    return iterator(position.position->nextNode);
	}
};

template <typename T>
class LinkedList<T>::iterator: public std::iterator<std::forward_iterator_tag, T>
{
    friend LinkedList;
    Node* position;

    public:
        friend class LinkedList;

        iterator():position(0)
        {}

        iterator(Node* current):position(current)
        {}

        iterator& operator=(const iterator& otherIter)
        {
            position=otherIter.position;
            return *this;
        }

        reference operator->() const
        {
            return position->nextNode;
        }

        bool operator==(const iterator& otherIter) const
        {
            return position == otherIter.position;
        }

        bool operator!=(const iterator& otherIter) const
		{
			return position != otherIter.position;
		}

		iterator& operator++()
		{
			position = position->nextNode;
			return *this;
		}

        iterator operator++(int)
        {
            iterator old(*this);
            position = position->nextNode;
            return old;
        }

		reference operator*() const
		{
		    return position->data;
        }

};

template <typename T>
class LinkedList<T>::const_iterator : public std::iterator<std::forward_iterator_tag, const T>
{
    friend LinkedList;
	Node* position;
    public:

        const_iterator(): position(0)
        {}

        const_iterator(Node* current) : position(current)
        {}

        const_iterator(iterator it): position(it.position)
        {}

        const_iterator& operator++()
        {
            position = position->nextNode;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator old(*this);
            position = position->nextNode;
            return old;
        }

        reference operator*() const
        {
            return position->data;
        }

        bool operator!=(const const_iterator& otherIter) const
        {
            return position != otherIter.position;
        }

        const_iterator& operator=(const iterator& otherIter)
        {
            position=otherIter.position;
            return *this;
        }
};

#endif // LINKEDLIST_H
