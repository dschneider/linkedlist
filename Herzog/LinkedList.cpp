#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <typename T>                          //klasse f�r T importieren
class LinkedList
{
    struct Node {
        T value;                                //T f�r beliebigen Datentyp
        Node *next;                             //Knoten Pointer auf n�chstes Element
    };

    public:
        LinkedList();
        virtual ~LinkedList();

    protected:

    private:
        Node *first;                     //erster Knoten
};

#endif // LINKEDLIST_H
