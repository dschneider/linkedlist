/**
 *  File: LinkedList.h
 *
 *  Created on: Nov 06, 2016
 *  Author: Johannes Hackl MME16
 *
 *  Last modified on: Nov 25, 2016
 *
 *  C++ Template Function for a LinkedList
 */

#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED

#include <iostream>

template<typename T>
class LinkedList
{
public:

    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

private:
    struct Element
    {
        T data = T();
        Element* next = nullptr;
    };

    Element* headElementPtr = nullptr;

    inline void createFrontNode(const value_type& value);
    inline void createBackNode(const value_type& value);
    inline void clearList();

public:
    LinkedList() = default;

    LinkedList(const LinkedList& listObj)
    {
        for (auto elementOfListObj : listObj)
        {
            createBackNode(elementOfListObj);
        }
    }

    LinkedList(std::initializer_list<value_type> inputValues)
    {
        for (auto value : inputValues)
        {
            createBackNode(value);
        }
    }

    LinkedList operator=(const LinkedList& listObj) = delete;

    class iterator
    {
        friend typename LinkedList::const_iterator;

    protected:
        Element* iteratorPtr = nullptr;

    public:
        iterator() {}
        iterator(Element* pointer) : iteratorPtr(pointer) {}
        reference operator*()
        {
            return iteratorPtr->data;
        }

        Element* operator->()
        {
            return iteratorPtr;
        }

        iterator& operator++()
        {
            iteratorPtr = iteratorPtr->next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator tempIterator = iteratorPtr;
            iteratorPtr = iteratorPtr->next;
            return tempIterator;
        }

        bool operator!=(const iterator& compareIterator) const
        {
            return iteratorPtr != compareIterator.iteratorPtr;
        }

        bool operator==(const iterator& compareIterator) const
        {
            return iteratorPtr == compareIterator.iteratorPtr;
        }
    };

    class const_iterator
    {
    private:
        Element* iteratorPtr = nullptr;

    public:
        const_iterator() {}
        const_iterator(iterator nonConstIter) : iteratorPtr(nonConstIter.iteratorPtr) {}
        const_iterator(Element* pointer) : iteratorPtr(pointer) {}
        const_reference operator*()
        {
            return iteratorPtr->data;
        }

        Element* operator->()
        {
            return iteratorPtr;
        }

        const_iterator& operator++()
        {
            iteratorPtr = iteratorPtr->next;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator tempIterator = iteratorPtr;
            iteratorPtr = iteratorPtr->next;
            return tempIterator;
        }

        bool operator!=(const const_iterator& compareIterator) const
        {
            return iteratorPtr != compareIterator.iteratorPtr;
        }

        bool operator==(const const_iterator& compareIterator) const
        {
            return iteratorPtr == compareIterator.iteratorPtr;
        }

    };

    iterator begin()
    {
        return iterator(headElementPtr);
    }

    const_iterator begin() const
    {
        return const_iterator(headElementPtr);
    }

    bool empty() const
    {
        return (headElementPtr == nullptr);
    }

    reference front()
    {
        return headElementPtr->data;
    }

    const_reference front() const
    {
        return headElementPtr->data;
    }

    void push_front(value_type&& value)
    {
        createFrontNode(value);
    }

    void push_front(const value_type& value)
    {
        createFrontNode(value);
    }

    size_type size() const
    {
        size_type numOfElements = 0;

        for(const_iterator iter = (*this).begin(); iter!= (*this).end(); ++iter)
        {
            ++numOfElements;
        }
        return numOfElements;
    }

    iterator end()
    {
        return iterator(nullptr);
    }

    const_iterator end() const
    {
        return const_iterator(nullptr);
    }

    iterator erase_after(const_iterator currentElement)
    {
        if(headElementPtr)
        {
            Element* tempElementPtr = currentElement->next->next;
            delete currentElement->next;
            currentElement->next = tempElementPtr;
            return iterator(currentElement->next);
        }
        else
        {
            return (*this).end();
        }
    }

    ~LinkedList()
    {
        clearList();
    }
};

template<typename value_type>
void LinkedList<value_type>::createFrontNode(const value_type& value)
{
    Element* tempHeadElementPtr = headElementPtr;
    headElementPtr = new Element;
    headElementPtr->data = value;
    headElementPtr->next = tempHeadElementPtr;
}

template<typename value_type>
void LinkedList<value_type>::createBackNode(const value_type& value)
{
    if (!headElementPtr)
    {
        createFrontNode(value);
    }
    else
    {
        Element* lastElementPtr = headElementPtr;
        while(lastElementPtr->next)
        {
            lastElementPtr = lastElementPtr->next;
        }
        lastElementPtr->next = new Element;
        lastElementPtr = lastElementPtr->next;
        lastElementPtr->data = value;
        lastElementPtr->next = nullptr;
    }
}

template<typename value_type>
void LinkedList<value_type>::clearList()
{
    while (headElementPtr)
    {
        Element* nextElement = headElementPtr->next;
        delete headElementPtr;
        headElementPtr = nextElement;
    }
}
#endif // LINKEDLIST_H_INCLUDED
