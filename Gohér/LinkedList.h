#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>


template <typename T>
class LinkedList
{
public:
    //public types
    typedef T value_type;
    typedef value_type &reference;
    typedef const value_type &const_reference;
    typedef std::size_t size_type;

    //private create node
private:
    struct node
    {
        value_type val;
        node* next;
    };

    node* head;

public:
    class iterator;
    class const_iterator
    {
        friend LinkedList;

    private:
        node* nIterator;

    public:
        const_iterator(node* nInput)
        {
            nIterator = nInput;
        }

        const_iterator(const iterator& other);

        const_iterator operator++()
        {
            nIterator = nIterator->next;

            return nIterator;
        }

        const_iterator operator++ (int)
        {
            const_iterator tmp = nIterator;

            nIterator = nIterator->next;
            return tmp;
        }

        const_reference operator* ()
        {
            return nIterator->val;
        }

        bool operator!=(const const_iterator &nInput) const
        {
            return nIterator != nInput.nIterator;
        }

    };

    class iterator
    {
        friend LinkedList;
    private:
        node* nIterator;

    public:
        iterator(node* nInput)        /*:nIterator(nInput){}*/
        {
            nIterator = nInput;
        }

        iterator operator++()
        {
            nIterator = nIterator->next;

            return nIterator;
        }

        iterator operator++ (int)
        {
            iterator tmp = nIterator;

            nIterator = nIterator->next;
            return tmp;
        }

        reference operator* ()
        {
            return nIterator->val;
        }

        bool operator!=(const iterator &nInput) const
        {
            return nIterator != nInput.nIterator;
        }

        bool operator==(const iterator &nInput) const
        {
            return nIterator == nInput.nIterator;
        }

    };

    //constructor
    LinkedList()
    {
        head=nullptr;

    }

    LinkedList(const std::initializer_list<int> &v) : head(nullptr)
    {

        node* curr = nullptr;

        for (auto item : v)
        {
            node* newnode = new node;
            newnode->next = nullptr;
            newnode->val = item;

            if(head == nullptr)
            {
                head = newnode;
                curr = newnode;
            }
            else
            {
                curr->next = newnode;
                curr = newnode;

            }
        }
    }

    //destructor
    ~LinkedList()
    {
        node* tmp = head;

        while(tmp!=nullptr)
        {
            head = head->next;
            delete(tmp);
            tmp = head;
        }

    }

    //public functions
    void push_front(value_type &&value)
    {
        node* newnode = new node;
        newnode->next = head;
        head = newnode;
        head->val = value;
    }

    void push_front(const value_type &value)
    {
        node* newnode = new node;
        newnode->next = head;
        head = newnode;
        head->val = value;
    }

    reference front()
    {
        return *begin();
    }

    const_reference front() const
    {
        return *begin();
    }

    bool empty () const
    {
        return(head==nullptr);
    }

    size_type size() const
    {
        size_type cnt = 0;
        node* tmp = head;
        while(tmp!=nullptr)
        {
            tmp = tmp->next;
            cnt++;
        }
        return cnt;
    }

    iterator begin()
    {
        iterator itBegin(head);
        return itBegin;
    }
    iterator end()
    {
        /*node* tmp = head;

        for(uint16_t i=1; i<size(); i++)
        {
            tmp = tmp->next;
        }


        iterator itEnd(tmp);*/

        iterator itEnd(nullptr);

        return itEnd;
    }

    const_iterator begin() const
    {
        const_iterator itBegin(head);
        return itBegin;
    }
    const_iterator end() const
    {
        /*node* tmp = head;

        for(uint16_t i=1; i<size(); i++)
        {
            tmp = tmp->next;
        }

        const_iterator itEnd(tmp);*/

        const_iterator itEnd(nullptr);

        return itEnd;
    }

    //uebung erase after
    iterator erase_after(const_iterator position)
    {
        node* tmp = position.nIterator->next;
        if(position.nIterator != nullptr)
        {

            if (position.nIterator->next)
            {
                position.nIterator->next = position.nIterator->next->next;
                delete tmp;
            }
            iterator new_position = position.nIterator->next;
            if(new_position.nIterator != nullptr)
            {
                return new_position;
            }
            else
            {
                return end();
            }

        }
        else
        {
            return end();
        }

    }

    void print()
    {
        std::cout<<std::endl;
        node* tmp = head;
        int y =1;
        if(tmp==nullptr)
        {
            std::cout<<"List is empty"<<std::endl;
        }
        else
        {
            while(tmp!=nullptr)
            {
                std::cout<<y<<". "<<tmp->val<<std::endl;
                tmp=tmp->next;
                y++;
            }
        }
    }

};

template <class T>
LinkedList<T>::const_iterator::const_iterator(const iterator& other) : nIterator(other.nIterator)
{
}


#endif // LINKEDLIST_H
