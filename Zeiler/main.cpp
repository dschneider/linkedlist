#include "LinkedList.h"
#include <algorithm>
#include <numeric>
#include <iostream>

using namespace std;

template <typename T>   //das T erm�glicht die �bergabe eines beliebigen Datentyps
void print(const LinkedList<T> & x) {       //Methode zur Ausgabe, damit nicht immer cout... geschreiben werden muss
	copy(x.begin(), x.end(), std::ostream_iterator<T>(std::cout,"\n"));  // iterator, damit die Liste von Anfang bis Ende durchlaufen wird
}

int main() {
	LinkedList<int> list;    //Liste bauen
	list.push_front(13);     // push front--> eingef�gtes wird immer vorne an die liste angef�gt
	list.push_front(35);
	list.push_front(26);
	list.push_front(42);

    const int& value = 11;
    list.push_front(value);


	print(list);
    cout << "Groesse " << list.size() << endl;
    cout << endl;

}
