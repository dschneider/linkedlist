#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iterator>

template <typename T>
class LinkedList{

	struct Node{        //Struktur wird definiert, wie sieht knoten aus?
		Node(const T& value, Node* next = 0) : data(value), nextNode(next){}



		T data;         //attribute der struktur
		Node* nextNode; //attribute der struktur
	};

	Node* last;         //das ist ein attribut der klasse List

public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

    class iterator;
    class const_iterator;

	LinkedList() : last(new Node(T())) {
	    last->nextNode = last;     // Liste initialisieren und Komponetne nextNode verweist auf last
    }

	LinkedList(std::initializer_list<T> list) : last(new Node(T()))
	{
	    Node** next = &last->nextNode;
	    for (auto&& data : list)
	    {
	        Node* item = new Node(data, last);
            *next = item;
            next = &item->nextNode;
	    }
	}

	~LinkedList()
	{
	    while (last->nextNode != last)
	    {
	        auto to_delete = last->nextNode;
	        last->nextNode = to_delete->nextNode;
	        delete to_delete;
	    }
	    delete last;
	}

    void push_front(const T &data) {
	    last->nextNode = new Node(data, last->nextNode);
    }

	size_type size() const {
        size_type x =0;
        for(const_iterator i=begin(); i!=end(); ++i )
        {
            x++;
        }
        return x;
    }

	bool empty() const {
	    return last == last->nextNode;
    }

	T& front() {
	    return *begin();
    }

	const T& front() const {
	    return *begin();
    }

	iterator begin() {
	    return iterator(last->nextNode);
    }

	iterator end() {
	    return iterator(last);
    }

	const_iterator begin() const {
		return const_iterator(last->nextNode);
	}

	const_iterator end() const {
		return const_iterator(last);
	}

	iterator erase_after(const_iterator position)
	{
	    Node* to_delete = position.stelle->nextNode;
	    position.stelle->nextNode = to_delete->nextNode;
	    delete to_delete;
	    return iterator(position.stelle->nextNode);
	}
};

template <typename T>
class LinkedList<T>::iterator: public std::iterator<std::forward_iterator_tag, T>
{
	Node* stelle;

    public:
        friend class LinkedList;

        iterator():stelle(0) {}

        iterator(Node* current):stelle(current) {}

        iterator(const_iterator other) : stelle(other.stelle) {}

        iterator& operator=(const iterator& AndIter) {
            stelle=AndIter.stelle;
            return *this;
        }

        reference operator->() const {
            return stelle->nextNode;
        }

        bool operator==(const iterator& AndIter) const {
            return stelle == AndIter.stelle;
        }

        bool operator!=(const iterator& AndIter) const {
			return stelle != AndIter.stelle;
		}

		iterator& operator++() {
			stelle = stelle->nextNode;
			return *this;
		}

        iterator operator++(int) {
            auto old = iterator(*this);
            stelle = stelle->nextNode;
            return old;
        }

        reference operator*() const {
		    return stelle->data;
        }

};

template <typename T>
class LinkedList<T>::const_iterator : public std::iterator<std::forward_iterator_tag, const T>
{
    friend LinkedList;
	Node* stelle;
    public:

        const_iterator(): stelle(0) {}

        const_iterator(Node* current) : stelle(current) {}

        const_iterator(iterator other) : stelle(other.stelle) {}

        const_iterator& operator++() {
            stelle = stelle->nextNode;
            return *this;
        }

        const_iterator operator++(int) {
            auto old = const_iterator(*this);
            stelle = stelle->nextNode;
            return old;
        }

        reference operator*() const {
            return stelle->data;
        }

        bool operator!=(const const_iterator& AndIter) const {
            return stelle != AndIter.stelle;
        }

        const_iterator& operator=(const iterator& AndIter) {
            stelle=AndIter.stelle;
            return *this;
        }
};

#endif
