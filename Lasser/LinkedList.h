#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>

template <typename T> class LinkedList
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

private:
    struct TNode
    {
        value_type value;
        TNode* pNext;
    };

    TNode* head;

    TNode * MakeNode(int const value)
    {
        TNode * const pNewNode = new TNode;

        pNewNode->value = value;
        pNewNode->pNext = nullptr;

        return pNewNode;
    }

public:
    LinkedList() : head(nullptr)
    {
    }

    LinkedList(const std::initializer_list<int> &v) : head(nullptr)
    {
        TNode* curr = nullptr;
        TNode* newNode;

        for (auto item : v)
        {
            newNode = new TNode;
            newNode->pNext = nullptr;
            newNode->value = item;

            if(nullptr == head)
            {
                head = newNode;
                curr = head;
            }
            else
            {
                curr->pNext = newNode;
                curr = newNode;
            }
        }
    }

    class iterator
    {
    private:
        TNode* curr;

    public:
        friend class LinkedList;
        iterator(TNode* Node)
        {
            curr = Node;
        }

        iterator operator++()
        {
            curr = curr->pNext;
            return curr;
        }

        iterator operator++(int)
        {
            iterator tmp = curr;
            curr = curr->pNext;
            return tmp;
        }

        bool operator ==(const iterator &node) const
        {
            return(curr==node.curr);
        }

        bool operator !=(const iterator &node)
        {
            return(curr!=node.curr);
        }


        reference operator*() const
        {
            return curr->value;
        }
    };

    class const_iterator
    {
    private:
        TNode* curr;

    public:
        friend class LinkedList;
        const_iterator(iterator makeConst) : curr(makeConst.curr)
        {
        }

        const_iterator(TNode* Node)
        {
            curr = Node;
        }

        const_iterator operator++()
        {
            curr = curr->pNext;
            return curr;
        }

        bool operator !=(const const_iterator &node) const
        {
            return(curr!=node.curr);
        }

        const_iterator operator++(int)
        {
            const_iterator tmp = curr;
            curr = curr->pNext;
            return tmp;
        }

        const_reference operator*() const
        {
            return curr->value;
        }
    };


    //Zum warnings wegebekommen
    LinkedList(const LinkedList&) = delete;
    LinkedList& operator=(const LinkedList&) = delete;

    void push_front(value_type &&value)
    {
        TNode * pNewNode = MakeNode(value);

        pNewNode->pNext = head;
        head = pNewNode;
    }

    void push_front(const value_type &value)
    {
        TNode * pNewNode = MakeNode(value);

        pNewNode->pNext = head;
        head = pNewNode;
    }


    void push_back(value_type &&value)
    {
        TNode * pNewNode = MakeNode(value);
        TNode * pNode = head;
        TNode * pPrev = nullptr;

        if (head == nullptr)
        {
            head = pNewNode;
        }
        else
        {
            while (pNode != nullptr)
            {
                pPrev = pNode;
                pNode = pNode->pNext;
            }
            pPrev->pNext = pNewNode;
        }
    }

    reference front()
    {
        return *begin();
    }

    const_reference front() const
    {
        return *begin();
    }

    iterator begin()
    {
        return head; /* don't use implicite conversion */
    }

    iterator end()
    {
        return nullptr;
    }

    const_iterator begin() const
    {
        return head;
    }

    const_iterator end() const
    {
        return nullptr;
    }

    bool empty() const
    {
        return(head == nullptr);
    }

    size_type size() const
    {
        TNode *curr = head;
        int SizeOfList = 0;
        while(curr != nullptr)
        {
            curr = curr->pNext;
            SizeOfList++;
        }
        return SizeOfList;
    }

    void PrintList()
    {
        TNode *curr = head;
        int cnt = 1;

        if(empty())
        {
            std::cout << "LinkedList is empty!" << std::endl;
        }
        while (curr != nullptr)
        {
            std::cout << "Node " << cnt << ":\t";
            std::cout << curr->value << std::endl;
            cnt++;
            curr = curr->pNext;
        }
    }

    iterator erase_after(const_iterator position)
    {
        TNode *del = position.curr->pNext;
        iterator following(position.curr);

        while(position.curr != nullptr)
        {
            if (position.curr->pNext)
            {
                position.curr->pNext = position.curr->pNext->pNext;
                delete del;
                del = nullptr;
            }
            following = position.curr->pNext;
            if(following.curr != nullptr)
            {
                return following;
            }
            else
            {
                return end();
            }
        }
        return end();
    }

    //    void erase_after(int position)
//    {
//        int cnt = 0;
//        TNode * curr = head;
//        TNode * del = nullptr;
//
//        while (curr!=nullptr)
//        {
//            if (cnt == position)
//            {
//
//            }
//        }
//
//    }

    ~LinkedList()
    {
        TNode* del = head;
        while(head != nullptr)
        {
            head = head->pNext;
            delete(del);
            del = nullptr;
            del = head;
        }
    }
};

#endif // LINKEDLIST_H
