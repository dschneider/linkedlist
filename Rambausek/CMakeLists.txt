include_directories(.)
gtest(TARGET Rambausek_MasterTestLinkedList SOURCE ../MasterTestLinkedList.cpp)
gtest(TARGET Rambausek_TestLinkedList SOURCE TestLinkedList.cpp)

add_executable(Rambausek_Main main.cpp)
set_property(TARGET Rambausek_Main PROPERTY CXX_STANDARD 11)
