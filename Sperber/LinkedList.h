#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
#include <cstdlib>
#include <iostream>

template <typename T>
class LinkedList
{
private:

    LinkedList operator=(const LinkedList& copyList) = delete;
    LinkedList(const LinkedList& newList) = delete;

    typedef class node
    {
    public:
        node():next(nullptr) {}
        T data = 0;
        node* next;
    }* nodePtr;

    nodePtr head;
    nodePtr tail;



    class iterator_basis
    {
    public:
        typedef std::forward_iterator_tag iterator_category;
        typedef T value_type;
        typedef value_type & reference;
        typedef value_type* pointer;
        typedef std::size_t difference_type;
    };



public:

    typedef T value_type;
    typedef value_type &reference;
    typedef const value_type &const_reference;
    typedef size_t size_type;



    /************************** Iterator Class  ***************************/

    class iterator : public iterator_basis
    {
    private:
        nodePtr current;

    public:
        friend class LinkedList;
        iterator(nodePtr n = nullptr):current(n) {}
        iterator(const iterator& n):current(n.current) {}

        iterator operator=(const iterator& n)
        {
            current=n.current;
            return current;
        }

        iterator& operator++()
        {
            current = current->next;
            return *this;
        }
        iterator operator++(int)
        {
            iterator tmp(*this);
            current = current->next;
            return tmp;
        }
        reference operator*() const
        {
            return current->data;
        }

        bool operator==(const iterator& n) const
        {
            return current == n.current;
        }
        bool operator!=(const iterator& n) const
        {
            return current != n.current;
        }

    };


    /************************  Const Iterator Class  *************************/

    class const_iterator : public iterator_basis
    {
    private:
        nodePtr current;

    public:
        friend class LinkedList;
        const_iterator(const nodePtr n= nullptr):current(n) {}
        const_iterator(const const_iterator& n):current(n.current) {}
        const_iterator(const iterator& n):current(n.current){}

        const_iterator operator=(const const_iterator& n)
        {
            current=n.current;
            return current;
        }
        const_iterator& operator++()
        {
            current = current->next;
            return *this;
        }
        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            current = current->next;
            return tmp;
        }
        const_reference operator*() const
        {
            return current->data;
        }

        bool operator==(const const_iterator& n) const
        {
            return current == n.current;
        }
        bool operator!=(const const_iterator& n) const
        {
            return current != n.current;
        }

    };


    /************************  Constructor  ****************************/

    LinkedList(): head{nullptr},tail{nullptr} {}


    LinkedList(const std::initializer_list<T>& newList): head{nullptr},tail{nullptr}
    {
        for (auto i = newList.begin(); i != newList.end(); ++i)
        {
            push_back(*i);
        }

    }

    /*******************  LinkedList Methods  **************************/

    iterator begin()
    {
        return iterator(head);
    }

    iterator end()
    {
        return iterator();
    }

    const_iterator begin() const
    {
        return const_iterator(head);
    }

    const_iterator end() const
    {
        return const_iterator();
    }

    reference front()
    {
        return head->data;
    }

    const_reference front() const
    {
        return head->data;
    }

    void push_front(const T &newValue)
    {
        nodePtr current;
        nodePtr n = new node;
        n->next = nullptr;
        n->data = newValue;

        if(head != nullptr)
        {
            current = head;
            n->next = current;
            head = n;
        }

        else
        {
            head = n;
            tail = n;
        }
    }

    void push_front(T &&newValue)
    {
        nodePtr current;
        nodePtr n = new node;
        n->next = nullptr;
        n->data = newValue;

        if(head != nullptr)
        {
            current = head;
            n->next = current;
            head = n;
        }

        else
        {
            head = n;
            tail = n;
        }
    }

    size_type size() const
    {
        nodePtr current = head;
        size_type i = 0;
        while(current != nullptr)
        {
            i++;
            current = current->next;
        }
        return i;
    }

    bool empty() const
    {
        return begin() == end();
    }

    void push_back(const T &newValue)
    {
        nodePtr n = new node;
        n->next = nullptr;
        n->data = newValue;

        if(tail)
        {
            tail->next = n;
            tail = n;
        }

        else
        {
            head = n;
            tail = n;
        }
    }

    void push_back(const T &&newValue)
    {
        nodePtr n = new node;
        n->next = nullptr;
        n->data = newValue;

        if(tail)
        {
            tail->next = n;
            tail = n;
        }

        else
        {
            head = n;
            tail = n;
        }
    }

    /*******************  Additional Functions  **************************/

    reference back()
    {
        return tail->data;
    }

    void pop_front()
    {
        if (head)
        {
            node* tmp = head->next;
            delete head;
            head = tmp;
        }
    }

    void clearList()
    {
        while(head)
        {
            pop_front();
        }
    }

    iterator erase_after(const_iterator position)
    {
        nodePtr n = position.current->next;
        if (position.current->next)
            position.current->next = position.current->next->next;
        delete n;
        return position.current->next;
    }


    /************************  Deconstructor  ****************************/

    ~LinkedList()
    {
        clearList();
    };
};


#endif // LINKEDLIST_H_INCLUDED
