#include <iostream>
#include <initializer_list>
#ifndef LinkedList_h
#define LinkedList_h

template <typename T>
class LinkedList {
    public:
        typedef T value_type;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::size_t size_type;

    private:
        struct node {
            T data;
            node* next;
        };

    node* head;

    public:
        /*CONSTRUCTORS*/
        LinkedList() : head(NULL) {}
        LinkedList(std::initializer_list<value_type> list) : head(NULL) {
            node* currNode = NULL;
            node* newNode;

            for (value_type elem : list) {
                newNode = new node;
                newNode->next = NULL;
                newNode->data = elem;

                if(NULL == head) {
                    head = newNode;
                    currNode = newNode;
                }

                else {
                    currNode->next = newNode;
                    currNode = newNode;
                }
            }
        }

        /*DESTRUCTOR*/
        ~LinkedList() {
            node* currNode = head;

            while(NULL != currNode) {
                head = head->next;
                delete currNode;
                currNode = head;
            }
        }

        /*ITERATOR*/
        class iterator {
            friend LinkedList;
            private:
                node* actual;

            public:
                iterator(node* myNode) {
                    actual = myNode;
                }

                iterator operator++() {
                    actual = actual->next;
                    return actual;
                }

                iterator operator++(int) {
                    iterator foo = actual;
                    operator++();
                    return foo;
                }

                reference operator*() const {
                    return actual->data;
                }

                inline bool operator ==(const iterator& bar) const {
                    return actual == bar.actual;
                }

                inline bool operator !=(const iterator& bar) {
                    return actual != bar.actual;
                }
        };

        /*CONST ITERATOR*/
        class const_iterator {
            friend LinkedList;
            private:
            node* actual;

            public:
                const_iterator(node* myNode) {
                actual = myNode;
                }

                const_iterator(const iterator& other) : actual(other.actual)
                {
                }

                const_iterator operator++() {
                    actual = actual->next;
                    return actual;
                }

                const_iterator operator++(int) {
                    const_iterator foo = actual;
                    operator++();
                    return foo;
                }

                const_reference operator*() const {
                    return actual->data;
                }

                inline bool operator != (const const_iterator& bar) {
                    return actual != bar.actual;
                }
        };

        /*FUNCTION MEMBERS*/
        void push_front(value_type &&value) {
            node* newNode = new node;
            newNode->next = head;
            head = newNode;
            newNode->data = value;
        }

        void push_front(const value_type &value) {
            node* newNode = new node;
            newNode->next = head;
            head = newNode;
            newNode->data = value;
        }

        reference front() {
            return *begin();
        }

        const_reference front() const {
            return *begin();
        }

        bool empty() const {
            return (NULL == head);
        }

        size_type size() const {
            size_type valueSize = 0;
            node* currNode = head;

            while(currNode != NULL) {
                valueSize++;
                currNode = currNode->next;
            }

            return valueSize;
        }

        iterator begin() {
            iterator iter = iterator(head);
            return iter;
        }

        iterator end() {
            iterator iter = iterator(NULL);
            return iter;
        }

        const_iterator begin() const {
            const_iterator constIter = const_iterator(head);
            return constIter;
        }

        const_iterator end() const {
            const_iterator constIter = const_iterator(NULL);
            return constIter;
        }

       iterator erase_after(const_iterator position) {
            const_iterator currNode = begin();
            const_iterator tmpNode = currNode;

            ++position;
            while(currNode != position) {
                tmpNode = currNode++;

                if(NULL == currNode.actual->next) {
                        tmpNode.actual->next = currNode.actual->next;
                        delete currNode.actual;
                        return iterator(tmpNode.actual->next);
                }
            }

            if(currNode.actual->next == NULL) {
                   return iterator(currNode.actual);
            }

            tmpNode.actual->next = currNode.actual->next;
            delete currNode.actual;

            return iterator(tmpNode.actual->next);
       }

};

#endif
