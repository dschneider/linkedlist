#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
#include <iostream>
#include <cstdlib>
#include <iterator>

template <typename T>

class LinkedList
{
private:
    class node
    {
    public:
        T data;
        node* next;
    };
    typedef node* nodeptr;
    nodeptr head = nullptr;
    nodeptr current = nullptr;
    nodeptr tail = nullptr ;

    LinkedList operator=(const LinkedList newList) = delete;
    LinkedList(const LinkedList<T>& List) = delete;

public:
    typedef T value_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef size_t size_type;

    LinkedList()
    {
        head = nullptr;
        current = nullptr;
        tail = nullptr;
    }

    ~LinkedList()
    {
        clear_List();
    }

    class iterator_standard
    {
    public:
        typedef std::forward_iterator_tag iterator_category;
        typedef T value_type;
        typedef value_type & reference;
        typedef value_type* pointer;
        typedef std::size_t difference_type;
    };

    class iterator : public iterator_standard
    {
    private:
        nodeptr current_i=nullptr;
    public:
        friend class LinkedList;

        iterator(nodeptr n)
        {
            current_i = n;
        }

        iterator(const iterator& n)
        {
            current_i = n.current_i;
        }

        iterator operator=(const iterator& n)
        {
            current_i = n.current_i;
            return *this;
        }

        iterator& operator++()
        {
            current_i = current_i->next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator tmp(*this);
            current_i = current_i->next;
            return tmp;
        }

        reference operator*() const
        {
            return current_i->data;
        }
        bool operator==(const iterator& n) const
        {
            return current_i == n.current_i;
        }
        bool operator!=(const iterator& n) const
        {
            return current_i != n.current_i;
        }

    };

    class const_iterator : public iterator_standard
    {
    private:
        nodeptr current_i = nullptr;
    public:
        friend class LinkedList;

        const_iterator(const iterator& current_iterator)
        {
            current_i = current_iterator.current_i;
        }

        const_iterator(const nodeptr n)
        {
            current_i = n;
        }

        const_iterator(const const_iterator& n)
        {
            current_i = n.current_i;
        }

        const_iterator operator=(const const_iterator& n)
        {
            current_i = n.current_i;
            return *this;
        }

        const_iterator& operator++()
        {
            current_i = current_i->next;
            return *this;
        }

        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            current_i = current_i->next;
            return tmp;
        }

        const_reference operator*() const
        {
            return current_i->data;
        }
        bool operator==(const const_iterator& n) const
        {
            return current_i == n.current_i;
        }
        bool operator!=(const const_iterator& n) const
        {
            return current_i != n.current_i;
        }

    };

    LinkedList(const std::initializer_list<T>& constList)
    {
        head = nullptr;
        current = nullptr;
        tail = nullptr;

        for (auto iter = constList.begin(); iter!=constList.end(); ++iter)
        {
            push_back(*iter);
        }

    }

    void push_front(const T& value)
    {
        nodeptr n = new node;
        n->next = nullptr;
        n->data = value;

        if (head != nullptr)
        {
            current = head;
            n->next = current;
            head = n;
        }
        else
        {
            head = n;
            tail = n;
        }
    };

    void push_front(T&& value)
    {
        nodeptr n = new node;
        n->next = nullptr;
        n->data = value;

        if (head != nullptr)
        {
            current = head;
            n->next = current;
            head = n;
        }
        else
        {
            head = n;
            tail = n;
        }
    };

    void push_back(const T& value)
    {
        nodeptr n = new node;
        n->next = nullptr;
        n->data = value;

        if (tail != nullptr)
        {
            current = n;
            tail -> next = current;
            tail = current;
        }

        else
        {
            head = n;
            tail = n;
        }

    }
    void push_back(const T&& value)
    {
        nodeptr n = new node;
        n->next = nullptr;
        n->data = value;

        if (tail != nullptr)
        {
            current = n;
            tail -> next = current;
            tail = current;
        }

        else
        {
            head = n;
            tail = n;
        }

    }

    void clear_List ()
    {
        while (head != nullptr)
        {
            current = head->next;
            delete head;
            head = current;
        }
    }


    reference front()
    {
        return head->data;
    }

    const_reference front() const
    {
        return head->data;
    }

    size_type size() const
    {
        size_type s=0;
        nodeptr temp = head;
        while (temp != nullptr)
        {
            s++;
            temp = temp->next;
        }
        return s;
    }

    bool empty() const
    {
        return !head;
    }

     iterator erase_after(const_iterator position)
    {
        nodeptr n = position.current_i->next;
        if(position.current_i != nullptr)
        {

            if (position.current_i->next)
            {
                position.current_i->next = position.current_i->next->next;
                delete n;
            }
            iterator return_position = position.current_i->next;
            if(return_position.current_i != nullptr)
            {
                return return_position;
            }
            else
            {
                tail = position.current_i;
                return end();
            }

        }
        else
        {
            return end();
        }

    }
    iterator begin()
    {
        return iterator (head);
    }

    const_iterator begin() const
    {
        return const_iterator (head);
    }

    iterator end()
    {
        return iterator (tail->next);
    }

    const_iterator end() const
    {
        return const_iterator (tail->next);
    }

};

#endif // LINKEDLIST_H_INCLUDED
