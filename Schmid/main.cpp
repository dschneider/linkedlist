#include <iostream>
#include "LinkedList.h"

int main()
{
    LinkedList<int> Schmid;
    Schmid.push_front(2+1);
    Schmid.push_front(2);
    Schmid.push_front(1);
    Schmid.push_back(4);
    Schmid.push_back(5);
    Schmid.push_back(5+1);

//    for (LinkedList<int>::iterator iter=Schmid.begin(); iter != Schmid.end(); iter++ )
//    {
//        std::cout << *iter <<std::endl;
//    }


    std::cout << "Front: " << Schmid.front() << std::endl;
    std::cout << "Anzahl: " << Schmid.size() << std::endl;

//   std::cout << "Position: " << return_iterator << std::endl;

    std::cout << "Ausgabe der Liste:" << std::endl;
    for(auto value:Schmid)
    {
        std::cout << value <<std::endl;
    }

    if (Schmid.empty() == 0)
    {
        std::cout << "Liste ist befuellt" << std::endl;
    }
    else
    {
        std::cout << "Liste ist leer" << std::endl;
    }

    std::cout << "Die Liste wird umgedreht:" << std::endl;
    for(auto value:Schmid)
    {
        std::cout << value <<std::endl;
    }

    Schmid.clear_List();
    if (Schmid.empty() == 0)
    {
        std::cout << "Liste ist befuellt" << std::endl;
    }
    else
    {
        std::cout << "Liste ist leer" << std::endl;
    }

    const LinkedList<int> constList = {5,6,7,8};
    std::cout << "Die konstante Liste beinhaltet:" << std::endl;
    for(auto value:constList)
    {
        std::cout << value << std::endl;
    }


    return 0;
}
