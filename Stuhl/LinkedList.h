#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include<iostream>

// google: linkedlist.h c++
// google: c++ iterator class example


template <typename T>
class LinkedList
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

private:
    // create node
    class knot{
    public:
        T val;
        knot *next;
    };

    class knot *head;

public:
    LinkedList(){
        head=NULL;
    }

    LinkedList (std::initializer_list< value_type > list){
        head=NULL;
        class knot *temp;
        for(auto i:list){
            class knot *newknot=new knot;
            newknot->val=i;
            if(head==NULL){
                head=newknot;
                newknot->next=NULL;
            }else{
                temp->next=newknot;
                newknot->next=NULL;
            }
            temp=newknot;
        }
    }

    ~LinkedList(){
        while(head!=NULL){
            class knot *temp=head->next;
            delete(head);
            head=temp;
        }
    }

    class iterator{
    private:
        //knot *now;

    public:
        knot *now;
        iterator(knot *xyz){
            now=xyz;
        }

        class iterator& operator++ (){ // Pre-increment
            now = now->next;
            return *this;
        }

        class iterator operator++ (int){ // Post-increment
            knot *temp=now;
            now = now->next;
            return temp;
        }

        value_type& operator* () const{
            return now->val;
        }

        inline bool operator!=(const iterator& x) {
            return now != x.now;
        }

        inline bool operator==(const iterator& x) const {
            return now == x.now;
        }
    };

    class const_iterator{
    private:
        friend LinkedList;
        knot *now;
    public:
        const_iterator(knot *xyz){
            now=xyz;
        }

        const_iterator(const iterator& other) : now(other.now){
        }

        class const_iterator& operator++ (){ // Pre-increment
            now = now->next;
            return *this;
        }

        class const_iterator operator++ (int){ // Post-increment
            knot *temp=now;
            now = now->next;
            return temp;
        }

        value_type& operator* () const{
            return now->val;
        }

        inline bool operator!=(const const_iterator& x) {
            return now != x.now;
        }
    };

    void push_front(T &&value){    // && f�r nicht gespeicherte werte
        class knot *newknot=new knot;
        newknot->val=value;
        if(head==NULL){
            head=newknot;
            newknot->next=NULL;
        }else{
            newknot->next=head;
            head=newknot;
        }
    }

    void push_front(const T &value){
        class knot *newknot=new knot;
        newknot->val=value;
        if(head==NULL){
            head=newknot;
            newknot->next=NULL;
        }else{
            newknot->next=head;
            head=newknot;
        }
    }

    reference front(){
        class knot *temp=head;
        value_type *a=NULL;
        if(empty()){
            std::cout << "empty" << std::endl;
            return *a;
        }
        else{
            a=&temp->val;
            return *a;
        }
    }

    const_reference front()const{
        class knot *temp=head;
        value_type *a=NULL;
        if(empty()){
            std::cout << "empty" << std::endl;
            return *a;
        }
        else{
            a=&temp->val;
            return *a;
        }
    }

    bool empty()const{
        return(head==NULL);
    }

    size_type size()const{
        class knot *temp=head;
        size_type a=0;
        if(head!=NULL){
            while(temp!=NULL){
                a++;
                temp=temp->next;
            }
        }
        return a;
    }

    iterator begin(){
        return iterator(head);
    }

    iterator end(){
        class knot *cur=head;
        while (cur!=NULL){
            cur=cur->next;
        }
        return cur;
    }

    const_iterator begin()const{
        return const_iterator(head);

    }

    const_iterator end()const{
        class knot *cur=head;
        while (cur!=NULL){
            cur=cur->next;
        }
        return cur;
    }

    void print(){
        class knot *temp=head;
        int i=0;
        if(empty()){
            std::cout << "empty" << std::endl;
            return;
        }
        while(temp!=NULL){
            i++;
            std::cout << i << ".value: " << temp->val << std::endl;
            temp=temp->next;
        }
    }

    iterator erase_after(const_iterator position){
        const_iterator cur=begin();
        const_iterator temp=cur;

        position++;
        while(cur!=position){
            temp=cur++;
            if(cur.now->next==NULL){
                temp.now->next=NULL;
                delete cur.now;
                return iterator(temp.now->next);
            }
        }
        if(cur.now==head){
            head=cur.now->next;
            delete &cur;
            return head;
        }

        temp.now->next=cur.now->next;
        delete cur.now;
        return iterator(temp.now->next);
    }
};

#endif // LINKEDLIST_H
