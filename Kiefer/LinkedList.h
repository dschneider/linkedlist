//
//  LinkedList.h
//  LinkedList
//
//  Created by Daniel Kiefer on 02.11.16.
//  Copyright © 2016 Daniel Kiefer. All rights reserved.
//

#include <iostream>
#include <initializer_list>
#include <vector>
#ifndef LinkedList_h
#define LinkedList_h

template <typename T> class LinkedList
{
public:
    //Typedefs
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

private:
    struct node
    {
        value_type data;
        node* next;
    };

    node* head;

public:

    //cons:
    LinkedList() : head(nullptr)
    {

    }

    LinkedList(const std::initializer_list<value_type> list) : head(nullptr)
    {
        node* curr = nullptr;
        node* n;
        for (auto item : list) {

            n = new node;
            n->next = nullptr;
            n->data = item;

            if(nullptr == head)
            {
                head = n;
                curr = n;
            }
            else
            {
                curr->next = n;
                curr = n;
            }
        }
    }

    //des:
    ~LinkedList()
    {
        node* curr = head;
        while(nullptr != curr)
        {
            head = head->next;
            delete curr;
            curr = head;
        }
    }

    //iterators

    class iterator
    {
    private:
        node* now;

    public:
        iterator(node* mynode)
        {
            now = mynode;
        }

        iterator operator++()           //Pre-Increment
        {
            //if(nullptr == now)
            //    return nullptr;
            now = now->next;
            return now;
        }

        iterator operator++(int)        //Post-Increment
        {
            //if(nullptr == now)
            //    return nullptr;
            iterator tmp = now;
            operator++();
            return tmp;
        }

        reference operator*() const     //Datenzugriff
        {
            return now->data;
        }

        bool operator !=(const iterator &b) const   //Vergleich von Iteratoren
        {
            return (now != b.now);
        }

        bool operator ==(const iterator &b) const   //Vergleich von Iteratoren
        {
            return (now == b.now);
        }
        friend class LinkedList;
    };

    class const_iterator
    {
    private:
        friend class LinkedList;
        node* now;

    public:
        const_iterator(node* mynode)
        {
            now = mynode;
        }

        const_iterator(iterator it)
        {
            now = it.now;
        }

        const_iterator operator++()         //Pre-Increment
        {
            //if(nullptr == now)
            //   return nullptr;
            now = now->next;
            return now;
        }

        const_iterator operator++(int)      //Post-Increment
        {
            //if(nullptr == now)
            //    return nullptr;
            const_iterator tmp = now;
            operator++();
            return tmp;
        }

        const_reference operator*() const         //Datenzugriff
        {
            return now->data;
        }

        bool operator !=(const const_iterator &b) const   //Vergleich von Iteratoren
        {
            return (now != b.now);
        }

        bool operator ==(const const_iterator &b) const   //Vergleich von Iteratoren
        {
            return (now == b.now);
        }
    };

    /*aus forward_list ab Zeile 326
     _LIBCPP_INLINE_VISIBILITY
     __forward_list_iterator& operator++()
     {
     __ptr_ = __traits::__as_iter_node(__ptr_->__next_);
     return *this;
     }
     _LIBCPP_INLINE_VISIBILITY
     __forward_list_iterator operator++(int)
     {
     __forward_list_iterator __t(*this);
     ++(*this);
     return __t;
     }
     */

    //functions:

    void push_front(value_type &&value)
    {
        node* n = new node;
        n->next = head;
        head = n;
        n->data = value;
    }

    void push_front(const value_type &value)
    {
        node* n = new node;
        n->next = head;
        head = n;
        n->data = value;
    }

    reference front()
    {
        return *begin();
    }

    const_reference front() const
    {
        return *begin();
    }

    bool empty() const
    {
        return (nullptr == head);
    }

    //template <typename T> bool LinkedList<T>::empty() const
    //{
    //    return (nullptr == head);
    //}

    size_type size() const
    {
        size_type SizeValue = 0;
        node* current = head;
        while(current != nullptr)
        {
            SizeValue++;
            current = current->next;
        }

        return SizeValue;
    }

    iterator begin()
    {
        iterator a = iterator(head);
        return a;
    }

    iterator end()
    {
        iterator a = iterator(nullptr);
        return a;
    }

    const_iterator begin() const
    {
        const_iterator a = const_iterator(head);
        return a;
    }

    const_iterator end() const
    {
        const_iterator a = const_iterator(nullptr);
        return a;
    }

    ////////////////////////////////////
    //eigene Funktionen:

    void PushBack(value_type addvar)
    {
        node* curr;
        node* n = new node;
        n->next = nullptr;
        n->data = addvar;

        //n in der List einfügen
        if(nullptr == head)
        {
            head = n;
        }
        else //if(nullptr != head)
        {
            curr = head;
            while(curr->next != nullptr)
            {
                curr = curr->next;
            }
            curr->next = n;

        }

    }

    iterator erase_after(const_iterator position)
    {
        iterator position_before((position++).now);
        iterator position_after(position.now);
        position_after++;
        position_before.now->next=position_after.now;
        delete(position.now);
        return position_after;
    }


    //    void DeleteNode(int del)
    //    {
    //        node* n = NULL;
    //        temp = head;
    //        curr = head;
    //
    //
    //    }

    void PrintNodes()
    {
        node* curr;
        curr = head;
        if(nullptr == curr)
            std::cout << "Linked List is empty " << std::endl << std::endl;
        else
        {
            std::cout << std::endl << "Linked List: " << std::endl;
            while(nullptr != curr)
            {
                std::cout << curr->data << std::endl;
                curr = curr->next;
            }
            std::cout << std::endl;
        }
    }

    void EraseNodes()
    {
        node* curr;
        while(nullptr != head)
        {
            curr = head;
            head = head->next;
            delete(curr), curr = nullptr;
        }
    }

};


#endif /* LinkedList_h */



