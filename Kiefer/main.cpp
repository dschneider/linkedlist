//
//  main.cpp
//  LinkedList
//
//  Created by Daniel Kiefer on 02.11.16.
//  Copyright © 2016 Daniel Kiefer. All rights reserved.
//

#include <iostream>

#include <gtest/gtest.h>

#include "LinkedList.h"

int main() {

    LinkedList<int>* myListInt5 = new LinkedList<int> {77,88,99,111};
    //manueller Test - im Testfile nichts geändert, deshalb nicht abgegeben
    auto lala = myListInt5->begin();
    auto bela = myListInt5->erase_after(lala);
    std::cout << std::endl << "bela: " <<  *bela << std::endl;

    return 0;
}
