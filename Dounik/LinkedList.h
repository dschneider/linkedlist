#ifndef LINKEDLIST_H_INCLUDED
#define LINKEDLIST_H_INCLUDED
#include <iostream>


using namespace std;

template <typename T>
class LinkedList
{

public:

    class Node
    {
    public:

        Node(const T &data) : val(data), next(NULL) {}
        T      val;
        Node  *next;
    };


    LinkedList(const LinkedList& List) = delete;

    LinkedList operator=(const LinkedList& x) = delete;

    typedef Node* nodePtr;
    typedef Node const * constNodePtr;

    nodePtr head = NULL;
    nodePtr curr = NULL;
    nodePtr tail = NULL ;

    LinkedList()
    {
        head = NULL;
        tail = NULL;
        curr = NULL;
    }

    ~LinkedList()
    {
        clear_LinkedList();
    }

    LinkedList(const initializer_list<T>& constList)
    {
        head = NULL;
        curr = NULL;
        tail = NULL;

        for (auto iter = constList.begin(); iter!=constList.end(); ++iter)
        {
            push_back(*iter);
        }
    }

    class iterator
    {

    private:

    public:
        nodePtr point_to_node;
        iterator(nodePtr data=NULL):point_to_node(data) {}
        iterator& operator++()
        {
            point_to_node = point_to_node->next;
            return *this;
        }

        iterator operator++(int)
        {
            iterator tmp(*this);
            point_to_node = point_to_node->next;
            return tmp;
        }

        bool operator==(const iterator &data) const
        {
            return point_to_node == data.point_to_node;
        }

        bool operator!=(const iterator &data) const
        {
            return point_to_node != data.point_to_node;
        }
        const int& operator*() const
        {
            return point_to_node->val;
        }
    };

    class const_iterator
    {

    public:
        nodePtr point_to_node;
        const_iterator(const nodePtr data=NULL):point_to_node(data) {}
        const_iterator(const iterator& other):point_to_node(other.point_to_node) {}
        const_iterator& operator++()
        {
            point_to_node = point_to_node->next;
            return *this;
        }
        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            point_to_node = point_to_node->next;
            return tmp;
        }
        bool operator==(const const_iterator &data) const
        {
            return point_to_node ==data.point_to_node;
        }
        bool operator!=(const const_iterator &data) const
        {
            return point_to_node !=data.point_to_node;
        }
        const int& operator*() const
        {
            return point_to_node->val;
        }


    };

    typedef T value_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef size_t size_type;
    void push_front (T &&data);
    void push_front (const T &data);
    void push_back (const T &data);
    reference front ();
    const_reference front () const;
    bool empty () const;
    size_type size () const;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    void clear_LinkedList();
    iterator erase_after(const_iterator position);

};
template <typename T>
void LinkedList<T>::push_front(T &&value)
{
    nodePtr tmp = head;
    head = new Node(value);
    head->next = tmp;

    if ( !tail )
    {
        tail = head;
    }
}

template <typename T>
void LinkedList<T>::push_front(const T &value)
{
    nodePtr tmp = head;
    head = new Node(value);
    head->next = tmp;

    if ( !tail )
    {
        tail = head;
    }
}

template <typename T>
void LinkedList<T>::push_back(const T& val)
{
    nodePtr tmp = new Node(val);
    tmp->next = nullptr;
    tmp->val = val;

    if (tail != nullptr)
    {
        curr = tmp;
        tail -> next = curr;
        tail = curr;
    }

    else
    {
        head = tmp;
        tail = tmp;
    }
}



template <typename T>
typename LinkedList<T>::reference LinkedList<T>::front()
{
    if ( !head )
    {
        std::cout << "List is Empty!" << std::endl;
    }

    return head->val;
}


template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::front() const
{
    if ( !head )
    {
        std::cout << "List is Empty!" << std::endl;
    }

    return head->val;
}
template <typename T>
bool LinkedList<T>::empty() const
{
    return ( head == NULL );
}

template <typename T>
typename LinkedList<T>::size_type LinkedList<T>::size() const
{
    unsigned int size = 0;
    for ( nodePtr curr = head; curr; curr = curr->next)
    {
        ++size;
    }

    return size;
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    return iterator(head);
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    return iterator(tail->next);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::begin() const
{
    return const_iterator(head);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::end() const
{
    return const_iterator(tail->next);
}

template <typename T>
void LinkedList<T>::clear_LinkedList()
{
    while(head!=nullptr)
    {
        curr = head->next;
        delete head;
        head = curr;
    }
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::erase_after(const_iterator position)
{
    if(position.point_to_node->next!=NULL)
    {
        nodePtr tmp = position.point_to_node->next->next;
        delete position.point_to_node->next;
        position.point_to_node->next = tmp;
        if (tmp == nullptr)
        {
            tail = position.point_to_node;
        }
        return iterator(position.point_to_node->next);
    }
    else
    {
        return iterator(tail->next);
    }

}

#endif // LINKEDLIST_H_INCLUDED
