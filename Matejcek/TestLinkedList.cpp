/**
 * @file TestLinkedList.cpp
 * @brief The tests for class LinkedList
 */

#include <array>
#include <gmock/gmock.h>

#include "LinkedList.h"

#define ANY_INT_VALUE 4

class Variable
{
public:
    typedef LinkedList<int> ListType;
};

class Const
{
public:
    typedef typename std::add_const<Variable::ListType>::type ListType;
};

class Empty
{
};

class Filled
{
};

template<typename VariablePolicy, typename DataPolicy>
class TestFixtureList: public ::testing::Test
{
public:
    typename VariablePolicy::ListType list;

    template<typename U=void>
    TestFixtureList();
};

template<typename VariablePolicy, typename DataPolicy>
template<typename U>
TestFixtureList<VariablePolicy, DataPolicy>::TestFixtureList() :
        list()
{
}

template<typename VariablePolicy>
class TestFixtureList<VariablePolicy, Filled> : public ::testing::Test
{
public:
    typename VariablePolicy::ListType list;
    TestFixtureList() :
            list(
            { 5, 3, 7 })
    {
    }
};

typedef TestFixtureList<Const, Empty> TestListConstEmpty;
typedef TestFixtureList<Variable, Empty> TestListEmpty;
typedef TestFixtureList<Const, Filled> TestListConst;
typedef TestFixtureList<Variable, Filled> TestList;
typedef TestFixtureList<Variable, Filled> TestAddFunction;

TEST_F(TestListConstEmpty, is_empty)
{
    ASSERT_TRUE(list.empty());
}

TEST_F(TestListConstEmpty, has_size_zero)
{
    ASSERT_EQ(0, list.size());
}

TEST_F(TestListEmpty, prepend_value)
{
    list.push_front(ANY_INT_VALUE);

    ASSERT_EQ(list.front(), ANY_INT_VALUE);
}

TEST_F(TestListEmpty, prepend_const_ref)
{
    const int& value = ANY_INT_VALUE;

    list.push_front(value);

    ASSERT_EQ(list.front(), ANY_INT_VALUE);
}

TEST_F(TestListConst, get_first_const_value)
{
    ASSERT_EQ(list.front(), 5);
}

TEST_F(TestList, get_first_value)
{
    ASSERT_EQ(list.front(), 5);
}

TEST_F(TestListEmpty, size_should_be_one_after_adding_an_item)
{
    list.push_front(ANY_INT_VALUE);

    ASSERT_EQ(1, list.size());
}

TEST_F(TestList, size_should_increment_be_one_after_adding_an_item)
{
    auto old_size = list.size();
    list.push_front(ANY_INT_VALUE);

    ASSERT_EQ(old_size + 1, list.size());
}

TEST_F(TestList, begin_should_return_an_iterator_to_first_element)
{
    LinkedList<int> list;
    list.push_front(ANY_INT_VALUE);

    ASSERT_EQ(*list.begin(), ANY_INT_VALUE);
}

TEST_F(TestList, post_increment_iterate)
{
    decltype(list)::iterator it = list.begin();
    ASSERT_EQ(5, *it++);
    ASSERT_EQ(3, *it);
}

TEST_F(TestListConst, post_increment_iterate)
{
    decltype(list)::const_iterator it = list.begin();
    ASSERT_EQ(5, *it++);
    ASSERT_EQ(3, *it);
}

TEST_F(TestList, pre_increment_iterate)
{
    decltype(list)::iterator it = list.begin();
    ASSERT_EQ(3, *(++it));
    ASSERT_EQ(3, *it);
}

TEST_F(TestListConst, pre_increment_iterate)
{
    decltype(list)::const_iterator it = list.begin();
    ASSERT_EQ(3, *(++it));
    ASSERT_EQ(3, *it);
}

TEST_F(TestList, loop_range_based)
{
    std::array<int, 3> expected_values =
    { 5, 3, 7 };
    auto expected = expected_values.begin();

    for (auto value : list)
    {
        ASSERT_EQ(value, *expected++);
    }
}

TEST_F(TestList, assert_that)
{
    ASSERT_THAT(list, testing::ElementsAre(5, 3, 7));
}

//-----------------------------------------------------
TEST_F(TestAddFunction, erase_after)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),5);
    list.erase_after(it);
    ASSERT_THAT(list, testing::ElementsAre(5, 7));
}
TEST_F(TestAddFunction, erase_after_returnVal)
{
    LinkedList<int>::iterator it = list.begin();
    LinkedList<int>::iterator expected;
    expected = list.erase_after(it);
    ASSERT_EQ(*expected, 7);
}
TEST_F(TestAddFunction, erase_after_no_lastVal)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),3);
    LinkedList<int>::iterator expected;
    expected = list.erase_after(it);
    ASSERT_EQ(expected, list.end());
}
TEST_F(TestAddFunction, erase_after_lastVal)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),7);
    LinkedList<int>::iterator expected;
    expected = list.erase_after(it);
    ASSERT_EQ(expected, list.end());
}
TEST_F(TestAddFunction, erase_after_lastVal_list)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),7);
    list.erase_after(it);
    ASSERT_THAT(list, testing::ElementsAre(5, 3, 7));
}
//____________________________________________________________________________
// Folgende Testf�lle sollten noch erweitert werden nicht ausreichend getestet

TEST_F(TestAddFunction, insert_after)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),3);
    list.insert_after(it, 10);
    ASSERT_THAT(list, testing::ElementsAre(5, 3, 10 ,7));
}

TEST_F(TestAddFunction, DeleteNode)
{
    LinkedList<int>::iterator it = std::find(list.begin(),list.end(),5);
    list.DeleteNode(it);
    ASSERT_THAT(list, testing::ElementsAre(3, 7));
}

TEST_F(TestAddFunction, push_back)
{
    list.push_back(10);
    ASSERT_THAT(list, testing::ElementsAre(5, 3, 7, 10));
}

TEST_F(TestAddFunction, pop_front)
{
    list.pop_front();
    ASSERT_THAT(list, testing::ElementsAre(3, 7));
}

TEST_F(TestAddFunction, delete_data)
{
    list.DeleteFirstNodeContaining(3);
    ASSERT_THAT(list, testing::ElementsAre(5, 7));
}

TEST_F(TestAddFunction, unique)
{
    list.push_front(1);
    list.push_front(1);
    list.push_front(1);
    list.unique();
    ASSERT_THAT(list, testing::ElementsAre(1, 5, 3, 7));
}

TEST_F(TestAddFunction, remove)
{
    list.push_front(1);
    list.push_front(3);
    list.push_front(1);
    list.push_back(1);
    list.push_front(1);
    list.push_back(1);
    list.push_back(3);
    list.push_back(1);
    list.remove(1);
    list.remove(3);
    ASSERT_THAT(list, testing::ElementsAre(5, 7));
}
