#ifndef LINKEDLIST_H
#define LINKEDLIST_H

using std::cout;
using std::endl;

template <typename T>
class LinkedList
{

private:
    class Node
    {
    public:
        Node() : next(nullptr) {}
        Node(const T& x,Node* y = nullptr):data(x),next(y) {}
        T data;
        Node* next;
    };
    typedef Node* NodePtr;
    typedef Node const * constNodePtr;
    NodePtr head = nullptr;

//    LinkedList operator=(const LinkedList copyList)
//    {    };
//    LinkedList (const LinkedList<T>& copyList)
//    {    };

    LinkedList operator=(const LinkedList copyList) = delete;
    LinkedList (const LinkedList<T>& copyList) = delete;

    class iterator_basis
    {
    public:
        typedef std::forward_iterator_tag iterator_category;
        typedef T value_type;
        typedef value_type & reference;
        typedef value_type* pointer;
        typedef std::size_t difference_type;
    };

public:
    typedef T value_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef size_t size_type;

    class iterator : public iterator_basis
    {
    private:
        NodePtr current = nullptr;

    public:
        friend class LinkedList;
        iterator(NodePtr x=nullptr):current(x) {}
        iterator(const iterator& old_iterator):current(old_iterator.current) {}

        iterator& operator=(const iterator& x)
        {
            current = x.current;
            return *this; // current;
        }

        iterator& operator++()
        {
            current = current->next;
            return /*current;*/*this;
        }
        iterator operator++(int)
        {
            iterator tmp(*this);
            current = current->next;
            return tmp;
        }
        reference operator*() const
        {
            return current->data;
        }
        bool operator==(const iterator& x) const
        {
            return current == x.current;
        }
        bool operator!=(const iterator& x) const
        {
            return current != x.current;
        }
    };

    class const_iterator : public iterator_basis
    {
    private:
        NodePtr current = nullptr;

    public:
        friend class LinkedList;
        const_iterator(const NodePtr x=nullptr):current(x) {}
        const_iterator(const const_iterator& old_iterator):current(old_iterator.current) {}

        const_iterator(const iterator& old_iterator):current(old_iterator.current){}

        const_iterator operator = (const const_iterator& x)
        {
            current=x.current;
            return current;
        }
        const_iterator& operator++()
        {
            current = current->next;
            return /*current;*/ *this;
        }
        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            current = current->next;
            return tmp;
        }
        const_reference operator*() const
        {
            return current->data;
        }
        bool operator==(const const_iterator& x) const
        {
            return current == x.current;
        }
        bool operator!=(const const_iterator& x) const
        {
            return current != x.current;
        }
    };

    LinkedList() : head(nullptr) {}

    LinkedList(const std::initializer_list<T>& L) : head(nullptr)
    {
        /*auto = typename std::initializer_list<T>::const_iterator*/
        for (auto i = L.begin(); i!=L.end(); ++i)
        {
            //push_back(*i);
            push_front(*i);
        }
        reverse();
    }

    void push_front(T&& addData)
    {
        NodePtr temp = new Node(addData);
        temp->next = head;
        head = temp;
    }

    void push_front(const T& addData)
    {
        NodePtr temp = new Node(addData);
        temp->next = head;
        head = temp;
    }

    bool empty() const
    {
        return !head;
    }

    reference front()
    {
        return *begin();
    }

    const_reference front() const
    {
        return *begin();
    }

    iterator begin()
    {
        return iterator(head);
    }

    iterator end()
    {
        return iterator();
    }

    const_iterator begin() const
    {
        return const_iterator(head);
    }

    const_iterator end() const
    {
        return const_iterator();
    }

    size_type size() const
    {
        size_type k = 0;
        NodePtr temp = head;
        while (temp != nullptr)
        {
            k++;
            temp = temp->next;
        }
        return k;
    }

    ~LinkedList()
    {
        clearList();
    };

    void reverse()
    {
        NodePtr actual = 0;
        NodePtr temp = head;
        NodePtr following;
        while (temp)
        {
            following = temp->next;
            temp->next = actual;
            actual = temp;
            temp = following;
        }
        head = actual;
    }

    void clearList()
    {
//        NodePtr temp = nullptr;
//        while (head != nullptr)
//        {
//            temp = head->next;
//            delete head;
//            head = temp;
//        }
        while(!empty())
        {
            pop_front();
        }
    }

    void pop_front()
    {
        if (head)
        {
            NodePtr newhead = head->next;
            delete head;
            head = newhead;
        }
    }
    //___________________________________________________________
    iterator erase_after(const_iterator position)
    {
        NodePtr temp = position.current->next;
        if (position.current->next)
        {
            position.current->next = position.current->next->next;
            delete temp;
        }
        iterator new_position = position.current->next;
        if(new_position.current != nullptr)
        {
            return new_position;
        }
        else
        {
            return end();
        }
    }
    //___________________________________________________________
    // -------------------------------------
    void push_back(const T& addData)
    {
        NodePtr temp = new Node(addData);
        if (head != nullptr)
        {
            NodePtr tail = head;
            while(tail->next != nullptr)
            {
                tail = tail->next;
            }
            tail->next = temp;
        }
        else
        {
            head = temp;
        }

    }

    void push_back(T&& addData)
    {
        NodePtr temp = new Node(addData);
        if (head != nullptr)
        {
            NodePtr tail = head;
            while(tail->next != nullptr)
            {
                tail = tail->next;
            }
            tail->next = temp;
        }
        else
        {
            head = temp;
        }
    }

    void insert_after(iterator& position, const T& insertData)
	{
		NodePtr temp = new Node(insertData, position.current->next);
		position.current->next = temp;
	}

    void unique(void)
    {
        NodePtr temp = head;
        NodePtr deletePtr = nullptr;

        while(temp->next != nullptr)
        {
            if(temp->data == temp->next->data)
            {
                deletePtr = temp->next;
                temp->next = temp->next->next;
                delete deletePtr;
            }
            else
            {
                temp = temp->next;
            }
        }
    }

    void remove(const T& deleteData)
    {
        NodePtr temp = head;
        NodePtr curr = head;
        NodePtr deletePtr = nullptr;

        while(curr->data == deleteData)
        {
            deletePtr = curr;
            curr = curr->next;
            head = curr;
            delete deletePtr;
        }

        while (curr != nullptr)
        {
            if (curr->data != deleteData)
            {
                temp = curr;
                curr = curr->next;
            }
            else
            {
                deletePtr = curr;
                curr = curr->next;
                temp->next = curr;
                delete deletePtr;
            }
        }
    }

    void DeleteNode(iterator& deleteNode)
    {
        NodePtr temp = head;
        if (temp->data == deleteNode.current->data)
        {
            head = deleteNode.current->next;
            delete temp;
        }
        else
        {
            while (temp->next != deleteNode.current)
            {
                temp = temp->next;
            }
            temp->next = deleteNode.current->next;
            temp = deleteNode.current;
            delete temp;
        }

    }

    bool DeleteFirstNodeContaining(const T& deleteData)
    {
        NodePtr deletePtr = nullptr;
        NodePtr curr = head;
        NodePtr temp = curr;

        while ((curr != nullptr) && (curr->data != deleteData))
        {
            temp = curr;
            curr = curr->next;
        }
        if (curr == nullptr)
        {
            delete deletePtr;
            return 0;
        }
        else if (curr == head)
        {
            deletePtr = curr;
            head = curr->next;
            delete deletePtr;
            return 1;
        }
        else
        {
            deletePtr = curr;
            curr = curr->next;
            temp->next = curr;
            delete deletePtr;
            return 1;
        }
    }

};

#endif // LINKEDLIST_H
