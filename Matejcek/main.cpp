#include <iostream>
#include "LinkedList.h"
#include <algorithm>
#include <numeric>

using std::cout;
using std::endl;

int main()
{
    LinkedList<int> Liste1;
    cout << "Liste leer? bool: " << Liste1.empty() <<endl;

    cout << "Liste wird befuellt " << endl;
    for (int i = 0; i < 6 ; i++)
    {
        Liste1.push_front(i);
    }
    Liste1.push_front(5+1);
    Liste1.push_back(7);

    cout << "Liste1 beinhaltet " << endl;
    for(auto value:Liste1)
    {
        cout << value << endl;
    }

    cout << "Liste leer? bool: " << Liste1.empty() <<endl << endl;

    cout << "Es wird nach '5' gesucht " << endl;
    LinkedList<int>::iterator it = std::find(Liste1.begin(),Liste1.end(),5);
    if (it != Liste1.end())
    {
        cout << "Gesucht & gefunden wurde " << *it << std::endl;
        LinkedList<int>::iterator return_it;
        return_it = Liste1.erase_after(it);
        cout << "Knoten danach wurde entfernt " << endl;
        cout << "Der Folgeknoten lautet " << *return_it << endl;
        Liste1.insert_after(it, 6);
        cout << "Nach dem gefundenen Knoten wurde eine 6 eingefuegt " << endl;
        Liste1.DeleteNode(it);
        cout << "Der gesuchte Knoten wurde entfernt " << endl;
    }

    Liste1.pop_front();
    cout << "Erstes Element wurde entfernt " << endl;

    if (Liste1.DeleteFirstNodeContaining(3))
    {
        cout << "Element mit Wert 3 wurde geloescht " << endl;
    }

    cout << "Liste1 beinhaltet " << endl;
    for (LinkedList<int>::iterator it = Liste1.begin(); it != Liste1.end(); ++it )
    {
        cout << *it << endl;
    }

    int nr = Liste1.size();
    cout << "Anzahl: " << nr << endl;

    cout << "Front: " << Liste1.front() << endl << endl << endl;


    const LinkedList<int> Liste2 = {4,7,9};
    cout << "Liste2 beinhaltet " << endl;
    for (auto value:Liste2)
    {
        cout << value << endl;
    }

    Liste1.clearList();
    cout << "Clear List wurde durchgefuehrt" << endl;
    cout << "Liste1 beinhaltet " << endl;
    for (auto value:Liste1)
    {
        cout << value << endl;
    }

    LinkedList<int> Liste3;
    {
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(2);
        Liste3.push_front(2);
        Liste3.push_front(2);
        Liste3.push_front(3);
        Liste3.push_front(2);
        Liste3.push_front(2);
        Liste3.push_front(7);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
        Liste3.push_front(1);
    }

    Liste3.remove(1);
    Liste3.unique();

    cout << "Liste3 beinhaltet " << endl;
    for (auto value:Liste3)
    {
        cout << value << endl;
    }

    return 0;
}
