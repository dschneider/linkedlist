#ifndef LINKEDLIST_H
#define LINKEDLIST_H

template <typename T>
class LinkedList
{
    struct Node
    {
        Node(const T &x):value(x),next(NULL) {}
        T value;
        Node* next;
    };
    Node* head;

public:
    class iterator
    {
        Node* m_node;

    public:
        friend class LinkedList;
        friend class const_iterator;

        iterator(Node* x=NULL):m_node(x) {}
        iterator(const iterator& x):m_node(x.m_node){}
        iterator& operator=(const iterator& x)
        {
            m_node = x.m_node;
            return *this;
        }
        iterator& operator++()
        {
            m_node = m_node->next;
            return *this;
        }
        iterator operator++(int)
        {
            iterator tmp(*this);
            m_node = m_node->next;
            return tmp;
        }
        bool operator==(const iterator &x) const
        {
            return m_node ==x.m_node;
        }
        bool operator!=(const iterator &x)
        {
            return m_node !=x.m_node;
        }
        const int& operator*() const
        {
            return m_node->value;
        }
    };
    class const_iterator
    {
        Node* m_node;

    public:
        friend class LinkedList;
        friend class iterator;

        const_iterator(Node* x=NULL):m_node(x) {}
        const_iterator(const const_iterator& x):m_node(x.m_node){}
        const_iterator(iterator& x):m_node(x.m_node){}
        const_iterator& operator=(const const_iterator& x)
        {
            m_node = x.m_node;
            return *this;
        }
        const_iterator(const iterator& x)
        {
            m_node = x.m_node;
        }
        const_iterator& operator++()
        {
            m_node = m_node->next;
            return *this;
        }
        const_iterator operator++(int)
        {
            const_iterator tmp(*this);
            m_node = m_node->next;
            return tmp;
        }
        bool operator==(const const_iterator &x)
        {
            return m_node ==x.m_node;
        }
        bool operator!=(const const_iterator &x)
        {
            return m_node !=x.m_node;
        }
        const int& operator*() const
        {
            return m_node->value;
        }
    };


    typedef T value_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef std::size_t size_type;


    LinkedList () : head(NULL) {}
    LinkedList (const LinkedList<value_type>&) = delete;
    LinkedList operator=(const LinkedList<value_type>&) = delete;
    LinkedList (std::initializer_list<T>list): head(NULL)
    {
        for(typename std::initializer_list<T>::const_iterator i=list.begin(); i!=list.end(); i++)
        {
            push_front(*i);
        }
        reverse();
    }
    ~LinkedList()
    {
        clear();
    }
    void reverse()
    {
        Node* new_node = NULL;
        Node* i = head;
        Node* n;

        while(i)
        {
            n=i->next;
            i->next=new_node;
            new_node=i;
            i=n;
        }
        head=new_node;
    }

    void clear(void)
    {
        while(!empty())
        {
           if(head)
            {
                Node* current = head ->next;
                delete head;
                head = current;
            }
        }
    }
    void push_front(T &&value);
    void push_front(const T &value);
    reference front();
    const_reference front() const;
    bool empty() const;
    size_type size() const;
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    iterator erase_after(const_iterator position);
};

template <typename T>
void LinkedList<T>::push_front(T &&value)
{
    if (head)
    {
        Node * tmp = new Node(value);
        tmp->next = head;
        head = tmp;
    }
    else
        head = new Node(value);
}

template <typename T>
void LinkedList<T>::push_front(const T & value)
{
    if (head)
    {
        Node * tmp = new Node(value);
        tmp->next = head;
        head = tmp;
    }
    else
        head = new Node(value);
}

template <typename T>
typename LinkedList<T>::reference LinkedList<T>::front()
{
    if(!head)
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->value;
}
template <typename T>
typename LinkedList<T>::const_reference LinkedList<T>::front() const
{
    if(!head)
    {
        std::cout <<"Can't return value from empty list!";
    }
    return head->value;
}

template <typename T>
bool LinkedList<T>::empty() const
{
    return (head == NULL);
}

template <typename T>
typename LinkedList<T>::size_type LinkedList<T>::size() const
{
    unsigned int size=0;
    for(Node *current = head; current; current = current->next)
    {
        ++size;
    }
    return size;
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    return iterator(head);
}

template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::begin() const
{
    return const_iterator(head);
}

template <typename T>
typename LinkedList<T>::const_iterator LinkedList<T>::end() const
{
    return const_iterator(NULL);
}
template <typename T>
typename LinkedList<T>::iterator LinkedList<T>::erase_after(const_iterator position)
{
    Node* tmp = position.m_node->next;
    if(position.m_node->next)
    {
        position.m_node->next =position.m_node->next->next;
    }
    delete tmp;
    return iterator(position.m_node->next);
}


#endif // LINKEDLIST_H
