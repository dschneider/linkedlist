#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>

template <class T>
class LinkedList
{
private:

    struct Node
    {
        Node(T newData)
        {
            data = newData;
            next = nullptr;
        }
        T data;
        Node* next;
    };

    Node* headNode;
    Node* endNode;
    Node* tempNode;

public:

    typedef T value_type;
    typedef value_type & reference;
    typedef const value_type & const_reference;
    typedef size_t size_type;


    LinkedList()
    {
        headNode = nullptr;
        endNode = nullptr;
        tempNode = nullptr;
    }

    LinkedList(const std::initializer_list<value_type>& L)
    {
        headNode = nullptr;
        endNode = nullptr;
        tempNode = nullptr;

        for (auto i = L.begin(); i!=L.end(); ++i)
        {
            push_back(*i);
        }
    }

    ~LinkedList()
    {
        clear();
    }

    void push_front(reference& newData)
    {
        Node* n = new Node(newData);
        n->next = headNode;
        if(headNode == nullptr)
        {
            endNode = n;
        }
        headNode = n;
    }

    void push_front(const_reference& newData)
    {
        Node* n = new Node(newData);
        n->next = headNode;
        if(headNode == nullptr)
        {
            endNode = n;
        }
        headNode = n;
    }

    void push_back(reference& newData)
    {
        Node* n = new Node(newData);

        if(headNode != nullptr)
        {
            tempNode = headNode;
            while(tempNode != endNode)
            {
                tempNode = tempNode->next;
            }
            tempNode->next = n;
        }
        else
        {
            headNode = n;
        }
        endNode = n;
    }

    void push_back(const_reference& newData)
    {
        Node* n = new Node(newData);

        if(headNode != nullptr)
        {
            tempNode = headNode;
            while(tempNode != endNode)
            {
                tempNode = tempNode->next;
            }
            tempNode->next = n;
        }
        else
        {
            headNode = n;
        }
        endNode = n;
    }

    bool empty() const
    {
        return !headNode;
    }

    reference front()
    {
        return *begin();
    }

    const_reference front() const
    {
        return *begin();
    }

    size_type size() const
    {
        Node* n = headNode;
        size_type counter = 0;
        while(n != nullptr)
        {
            counter++;
            n = n->next;
        }
        return counter;
    }

    void pop_front()
    {
        if (headNode)
        {
            Node* newHeadNode = headNode->next;
            delete headNode;
            headNode = newHeadNode;
        }
    }

    void clear()
    {
        while(!empty())
        {
            pop_front();
        }
    }

    // for testing
    void deleteData(reference& delData)
    {
        Node* delPtr = nullptr;
        tempNode = headNode;
        Node* currentNode = headNode;
        while(currentNode != nullptr && currentNode->data != delData)
        {
            tempNode = currentNode;
            currentNode = currentNode->next;
        }
        if(currentNode == nullptr)
        {
            std::cout << delData << " war nicht Teil der Liste." << std::endl;
            delete delPtr;      // let the memory free
        }
        else
        {
            delPtr = currentNode;
            currentNode = currentNode->next;
            tempNode->next = currentNode;
            if(delPtr == headNode)
            {
                headNode = headNode->next;
                tempNode = nullptr;
            }
            delete delPtr;
            std::cout << "Der Inhalt " << delData << " wurde aus der Liste entfernt." << std::endl;

        }
    }

    // for testing
    void deleteData(const_reference& delData)
    {
        Node* delPtr = nullptr;
        tempNode = headNode;
        Node* currentNode = headNode;
        while(currentNode != nullptr && currentNode->data != delData)
        {
            tempNode = currentNode;
            currentNode = currentNode->next;
        }
        if(currentNode == nullptr)
        {
            std::cout << delData << " war nicht Teil der Liste." << std::endl;
            delete delPtr;      // let the memory free
        }
        else
        {
            delPtr = currentNode;
            currentNode = currentNode->next;
            tempNode->next = currentNode;
            if(delPtr == headNode)
            {
                headNode = headNode->next;
                tempNode = nullptr;
            }
            delete delPtr;
            std::cout << "Der Inhalt " << delData << " wurde aus der Liste entfernt." << std::endl;

        }
    }

    // for testing
    void printList()
    {
        tempNode = headNode;
        while(tempNode != nullptr)
        {
            std::cout << tempNode->data << std::endl;
            tempNode = tempNode->next;
        }
    }

    class iterator
    {
        Node* currentIteratorNode = nullptr;
        friend class LinkedList;

    public:

        iterator(Node* newNodePtr = nullptr)
        {
            currentIteratorNode = newNodePtr;
        }

        iterator(const iterator& newIterator)
        {
            currentIteratorNode = newIterator.currentIteratorNode;
        }

        iterator& operator++()
        {
            currentIteratorNode = currentIteratorNode->next;
            return *this;
        }
        iterator operator++(int)
        {
            Node* temp = currentIteratorNode;
            currentIteratorNode = currentIteratorNode->next;
            return temp;
        }
        bool operator!=(iterator rval)
        {
            return !(currentIteratorNode == rval.currentIteratorNode);
        }
        bool operator==(iterator rval) const
        {
            return (currentIteratorNode == rval.currentIteratorNode);
        }
        value_type& operator*() const
        {
            return currentIteratorNode->data;
        }
    };

    class const_iterator
    {
        Node* currentIteratorNode = nullptr;
        friend class LinkedList;
        friend class iterator;

    public:

        const_iterator(Node* newNodePtr = nullptr)
        {
            currentIteratorNode = newNodePtr;
        }

        const_iterator(const iterator& newIterator)
        {
            currentIteratorNode = newIterator.currentIteratorNode;
        }

        const_iterator(const const_iterator& newIterator)
        {
            currentIteratorNode = newIterator.currentIteratorNode;
        }

        const_iterator& operator++()
        {
            currentIteratorNode = currentIteratorNode->next;
            return *this;
        }
        const_iterator operator++(int)
        {
            Node* temp = currentIteratorNode;
            currentIteratorNode = currentIteratorNode->next;
            return temp;
        }
        bool operator!=(const_iterator rval)
        {
            return !(currentIteratorNode == rval.currentIteratorNode);
        }
        bool operator==(const_iterator rval)
        {
            return (currentIteratorNode == rval.currentIteratorNode);
        }
        value_type& operator*() const
        {
            return currentIteratorNode->data;
        }
    };

    iterator begin()
    {
        return iterator(headNode);
    }

    iterator end()
    {
        return iterator();
    }

    const_iterator begin() const
    {
        return const_iterator(headNode);
    }

    const_iterator end() const
    {
        return const_iterator();
    }

    iterator erase_after(const_iterator position)
    {
        Node* tmp = position.currentIteratorNode->next;

        if (tmp)
        {
            position.currentIteratorNode->next = tmp->next;
        }
        delete tmp;
        return position.currentIteratorNode->next;
    }
};

#endif // LINKEDLIST_H
