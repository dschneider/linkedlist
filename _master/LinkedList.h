#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <initializer_list>

/**
 * @brief Singly Linked List
 */
template<typename T>
class LinkedList
{
private:
    struct Item
    {
        Item(Item* next, const T& value) :
                next(next), value(value)
        {
        }
        Item* next;
        T value;
    };

    Item* first;

public:
    typedef T value_type; /**< value type */

    typedef value_type& reference; /**< reference on value */
    typedef const value_type& const_reference; /**< constant reference on value */

    typedef std::size_t size_type; /**< Unsigned integer type */

    /**
     * @brief Forward iterator as in http://en.cppreference.com/w/cpp/concept/ForwardIterator
     */
    class iterator
    {
        friend LinkedList;
    private:
        iterator() :
                item(nullptr)
        {
        }

        iterator(Item* item) :
                item(item)
        {
        }

        Item* item;

    public:

        reference operator*()
        {
            return item->value;
        }

        /* post increment */
        iterator operator++(int)
        {
            iterator it(item);
            item = item->next;
            return it;
        }

        /* pre increment */
        iterator& operator++()
        {
            item = item->next;
            return *this;
        }

        bool operator==(const iterator& rhs) const
        {
            return this->item == rhs.item;
        }

        bool operator!=(const iterator& rhs) const
        {
            return this->item != rhs.item;
        }
    };

    /**
     * @brief Constant forward iterator as in http://en.cppreference.com/w/cpp/concept/ForwardIterator
     */
    class const_iterator
    {
        friend LinkedList;
    private:
        const_iterator() :
                item(nullptr)
        {
        }

        const_iterator(Item* item) :
                item(item)
        {
        }

        Item* item;

    public:
        const_iterator(iterator it) :
                item(it.item)
        {
        }

        reference operator*()
        {
            return item->value;
        }

        const_iterator operator++(int)
        {
            const_iterator it(item);
            item = item->next;
            return it;
        }

        const_iterator& operator++()
        {
            item = item->next;
            return *this;
        }

        bool operator!=(const const_iterator& rhs) const
        {
            return this->item != rhs.item;
        }
    };

    /**
     * @brief Default constructor to create an empty list
     */
    LinkedList() :
            first(nullptr)
    {
    }

    /**
     * @brief Constructor to create an initialized list
     * @param list Values to initialize to list
     */
    LinkedList(std::initializer_list<T> list) :
            first(nullptr)
    {
        Item** next = &first;
        for (auto&& value : list)
        {
            Item* item = new Item(nullptr, value);
            *next = item;
            next = &item->next;
        }
    }

    LinkedList(LinkedList&& other) = delete;
    LinkedList& operator=(const LinkedList& other) = delete;

    ~LinkedList()
    {
        while (first != nullptr)
        {
            Item* next = first->next;
            delete first;
            first = next;
        }
    }

    /**
     * @brief inserts an element to the beginning
     * @details Prepends the given element value to the beginning of the container.
     * No iterators or references are invalidated.
     * @param value the value of the element to prepend
     */
    void push_front(T&& value)
    {
        first = new Item(first, value);
    }

    /**
     * @brief Inserts an element to the beginning.
     * @details Prepends the given element value to the beginning of the container.
     * No iterators or references are invalidated.
     * @param value the value of the element to prepend
     */
    void push_front(const T& value)
    {
        first = new Item(first, value);
    }

    /**
     * @brief access the first element
     * @details Returns a reference to the first element in the container.
     * Calling front on an empty container is undefined.
     * @return Reference to the first element
     */
    reference front()
    {
        return first->value;
    }

    /**
     * @brief access the first element
     * @details Returns a reference to the first element in the container.
     * Calling front on an empty container is undefined.
     * @return Reference to the first element
     */
    const_reference front() const
    {
        return first->value;
    }

    /**
     * @brief checks whether the container is empty
     * @details Checks if the container has no elements, i.e. whether begin() == end().
     * @return true if the container is empty, false otherwise
     */
    bool empty() const
    {
        return true;
    }

    /**
     * @brief returns the number of elements
     * @return Number of element in the container
     */
    size_type size() const
    {
        size_type size = 0;
        for (Item* i = first; i != nullptr; i = i->next)
        {
            ++size;
        }
        return size;
    }

    /**
     * @brief Removes specified elements from the container.
     * @details Removes the element following @p position.
     * @param position before the element to remove
     * @return Iterator to the element following the erased one, or end() if no such element exists.
     */
    iterator erase_after(const_iterator position)
    {
        if (position.item)
        {
            auto to_delete = position.item->next;
            if (to_delete)
            {
                position.item->next = to_delete->next;
                delete to_delete;
                return iterator(position.item->next);
            }
        }
        return iterator();
    }

    /**
     * @brief returns an iterator to the beginning
     * Returns an iterator to the first element of the container.
     * If the container is empty, the returned iterator will be equal to end().
     * @return Iterator to the first element
     */
    iterator begin()
    {
        return iterator(first);
    }

    /**
     * @brief returns an iterator to the end
     * @details Returns an iterator to the element following the last element of the container.
     * This element acts as a placeholder; attempting to access it results in undefined behavior.
     * @return Iterator to the element following the last element.
     */
    iterator end()
    {
        return iterator();
    }

    /**
     * @brief returns an iterator to the beginning
     * Returns an iterator to the first element of the container.
     * If the container is empty, the returned iterator will be equal to end().
     * @return Iterator to the first element
     */
    const_iterator begin() const
    {
        return const_iterator(first);
    }

    /**
     * @brief returns an iterator to the end
     * @details Returns an iterator to the element following the last element of the container.
     * This element acts as a placeholder; attempting to access it results in undefined behavior.
     * @return Iterator to the element following the last element.
     */
    const_iterator end() const
    {
        return const_iterator();
    }

};

#endif /* LINKEDLIST_H */
